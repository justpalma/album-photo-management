# Album Photo
Album Photo is a Management Software for photo. It's a project made for OOP exam, and it was the first concrete project developed, and the first project made with java.

## What can be done
This project can create a simple album for photo. The album file will be a .bat, and it will stored on disk. An album contain a list of category, and every category can contain photo(jpg, png...) and GIF. A Category can be protected with a password. 

## Installation
```bash
cd src
```

```bash
java src/TestApp
```

The project is already compiled, and it is created with java 1.8. If you wanna perform some change and recompile it, make sure to use java version 8.
