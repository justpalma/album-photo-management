package src;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

	/**
	 * {@link JPanel} personalizzato per la modifica di una categoria
	 * 
	 * @see JPanel
	 * @author Palmieri Alessandro
	 */
public class PanelModificaCat extends JPanel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * {@link JTextField} in cui scrivere il nuovo nome della categoria
	 * @see JTextField
	 */
	private JTextField nome;
	/**
	 * {@link JTextArea} in cui scrivere la nuova descrizone della categoria
	 * @see JTextArea
	 */
	private JTextArea descrizione;
	
	/**
	 * inizializza il pannello
	 */
	public PanelModificaCat() {
		nome = new JTextField();
		nome.addAncestorListener(new FocusListener());
		descrizione = new JTextArea();
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		add(new JLabel("Nome categoria"));
		add(nome);
		add(new JLabel("Descrizione Categoria"));
		descrizione.setRows(5);
		descrizione.setColumns(20);
		descrizione.setLineWrap(true);
		JScrollPane scroll = new JScrollPane(descrizione);
		scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		scroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		add(scroll);		
	}
	
	/**
	 * Restituisce il nuovo nome della categoria
	 */
	public String getName() {
		return nome.getText();
	}
	
	/**
	 * Restituisce la nuova descrizione della categoria 
	 * @return stringa corrispondente alla descrizione, le andate a capo vengono sostituite da " " in modo da avere la descrizione su un unica riga
	 */
	public String getDescrizione() {
		String str = descrizione.getText();
		str=str.replaceAll("[\\r\\n]+", " ");
		return str;
	}
	
	/**
	 * imposta nel pannello il nome e la descrizione della categoria da modificare
	 * @param nome nome categoria
	 * @param descrizione descrizione categoria
	 */ 
	public void setPanel(String nome, String descrizione) {
		this.nome.setText(nome);
		this.descrizione.setText(descrizione);
	}

}
