package src;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;

	/**
	 * {@link JTabbedPane} personalizzato per visualizzare tutte le informazioni relative all'album
	 * @see JTabbedPane
	 * @author Palmieri Alessandro
	 *
	 */
public class PanelInfoAlbum extends JTabbedPane{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7590400048082400894L;
	
	/**
	 * Inizializza il pannello e visualizza tutte le informazioni dell'album passato
	 * @param album album da cui prendere informazioni
	 */
	public PanelInfoAlbum(Album album) {
		
		String prova = null;
		//System.out.println(FileSystems.getDefault().getSeparator());
		try {
			 prova = new File("resources"+FileSystems.getDefault().getSeparator()+"systemfoto").getCanonicalPath();  //necessario se eseguo da linea di comando
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		if(new File(prova).isDirectory()==false) {
			try {
				prova = new File("src"+FileSystems.getDefault().getSeparator()+"resources"+FileSystems.getDefault().getSeparator()+"systemfoto").getCanonicalPath();	//necessario se eseguo da eclipse
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		
		ImageIcon icon = new ImageIcon(prova+FileSystems.getDefault().getSeparator()+"info.jpg");
		int catpwd=0;
		int nfoto=0;
		JPanel pinfo = new JPanel();
		for(int i=0;i<album.getSizeAlbum();i++) {
			if(album.getCategoria(i) instanceof CategoriaPwd)
				catpwd++;
		}
		
		for(int i=0;i<album.getSizeAlbum();i++) {
			nfoto+=album.getCategoria(i).getSizeCategoria();
		}
		
		pinfo.setLayout(new GridLayout(5,2));
		pinfo.add(new JLabel("Nome album: "));
		pinfo.add(new JLabel(album.getNome(), SwingConstants.RIGHT));
		
		pinfo.add(new JLabel("Data creazione album: "));
		pinfo.add(new JLabel(album.getData().formatData(), SwingConstants.RIGHT));
		
		pinfo.add(new JLabel("Categorie totali: "));
		pinfo.add(new JLabel(Integer.toString(album.getSizeAlbum()), SwingConstants.RIGHT));
		
		pinfo.add(new JLabel("Categorie con password: "));
		pinfo.add(new JLabel(Integer.toString(catpwd), SwingConstants.RIGHT));
		
		pinfo.add(new JLabel("Numero foto totali: "));
		pinfo.add(new JLabel(Integer.toString(nfoto), SwingConstants.RIGHT));
		
		this.addTab("Informazioni Album",icon, pinfo, "Visualizza informazioni album");
		
		JPanel catinfo = new JPanel();
		String[] colonne = {"Nome Categoria", "Descrizione", "Password", "Data creazione","Numero Foto"};
		Object[][] dati = new Object[album.getSizeAlbum()][5];
		for(int i=0;i<album.getSizeAlbum();i++) {
			dati[i][0]=new String(album.getCategoria(i).getTitolo());
			dati[i][1]=new String(album.getCategoria(i).getDescrizione());
			if(album.getCategoria(i) instanceof CategoriaPwd)
				dati[i][2]=new String("Si");
			else
				dati[i][2]=new String("No");
			dati[i][3]=new String(album.getCategoria(i).getData().formatData());
			dati[i][4]=new String(Integer.toString(album.getCategoria(i).getImm().size()));
			
		}
		
		//creo modello per la jtable, 
		DefaultTableModel dtm = new DefaultTableModel(dati,colonne) {
			/**
			 * 
			 */
			private static final long serialVersionUID = -273908500947285460L;

			//override necessario in modo da avere le celle non editabili
			@Override
			public boolean isCellEditable(int row,int column)  {
				  return false;
				}
		};
		JTable tabella = new JTable(dtm);
		tabella.setFocusable(false);	
		tabella.setRowSelectionAllowed(false);	//celle non selezionabili
		tabella.getTableHeader().setReorderingAllowed(false);	//colonne e righe fisse, disabilitÓ la possibilitÓ di muoverle con il mouse
		tabella.setShowHorizontalLines(false);
		catinfo.setLayout(new BorderLayout());
		catinfo.add(tabella.getTableHeader(), BorderLayout.PAGE_START);
		catinfo.add(tabella,BorderLayout.CENTER);
		this.addTab("Informazioni Categorie",icon, catinfo, "Visualizza informazioni categorie");
		
	}

}




