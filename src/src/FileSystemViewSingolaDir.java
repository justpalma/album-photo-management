package src;

import java.io.File;
import java.io.IOException;

import javax.swing.filechooser.FileSystemView;

	/**
	 * Classe derivante da {@link FileSystemView} che gestisce un FileSystemView personalizzato.
	 * Esso crea un FileSystem a singola directory per l'utilizzo di un JFileChooser.
	 * In questo modo si obbliga l'utente a salvare i file in un indirizzo prestabilito.
	 * Viene bloccata la possibili� di creare sottodirectory, in modo da avere pi� semplice struttura per la directory scelta
	 * 
	 * @author Alessandro Palmieri
	 * @see FileSystemView
	 */

public class FileSystemViewSingolaDir extends FileSystemView {
	
	/**
	 * Percorso in cui salvare i file
	 */
	File root;
	
	/**
	 * Array di dimensione 1 in modo da non permettere all'utente di muoversi nella gerarchia di directory e bloccarlo
	 * nella directory {@link #root}
	 * @see #root
	 */
	File[] roots = new File[1];
	
	
	/**
	 * Inizializza un nuovo FileSystemView ma a singola directory, impostando {@link #root} secondo il parametro passato e bloccando
	 * la gerarchia di directory {@link #roots} sempre su {@link #root}
	 * @param dir {@link File} in cui "bloccare" il JFileChooser
	 * @see #root
	 * @see #roots
	 * @see File
	 */
	public FileSystemViewSingolaDir(File dir) {
		super();
		try {
			root=dir.getCanonicalFile();
			roots[0]=root;
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	@Override
	public File getDefaultDirectory() {
		return root;
	}

	@Override
	public File getHomeDirectory() {
		
		return root;
	}

	@Override
	public File[] getRoots() {
		return roots;
	}
	
	@Override
	public File createNewFolder(File containingDir) throws IOException {
		return null;
	}
	
	
	
	

}
