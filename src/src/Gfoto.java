package src;
import java.awt.AlphaComposite;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import javax.imageio.ImageIO;


	/**
	 * Classe che racchiude tutti i metodi necessari per la gestione delle immagini, dimensioni e file.
	 * Questa classe compente solo metodi statici e quindi non necessita di essere istanziata
	 * @author Alessandro Palmieri
	 *
	 */
public class Gfoto {
	
	/**
	 * Contiente tutti i formati di immagini che possono essere gestiti dall'album fotografico
	 */
	private static String formati[] = {"PNG","png","JPG","jpg", "JPEG", "jpeg", "png", "PNG", "gif", "GIF"};
	
	
	/**
	 * Controlla se il parametro passato � una foto che pu� essere gestita dal software
	 * @param f {@link File} da controllare
	 * @return true se il file passato come parametro � supportato
	 */
	public static boolean isFoto(File f) {
		int find=0;
		String ext=getExtension(f);
		for(int j=0;j<formati.length;j++) {
			if(ext.equals(formati[j]))
				find=1;
		}
		if(find==1) return true;
		return false;
	}
	
	/**
	 * Restituisce l'estensione del {@link File} passato. Se il parametro non ha un'estensione, ritorna una stringa vuota
	 * @param file {@link File} da cui estrarre l'estensione
	 * @return Estensione del file
	 */
	public static String getExtension(File file) {
        String fileName = file.getName();
        //controllo che "." non sia l'ultimo(0) e che sia presente nella stringa(-1)
        if(fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0)
        return fileName.substring(fileName.lastIndexOf(".")+1);		//restituisco il restante dopo l'ultimo "."
        else return "";
    }
	
	/**
	 * Restituisce l'estensione della stringa passata, che si presume sia associata ad un file.
	 * Se il parametro non ha un'estensione, ritorna una stringa vuota.
	 * @param s parametro da cui estrarre l'estensione
	 * @return estensione del file, stringa vuota nel caso in cui l'estensione non ci sia
	 */
	public static String getExtension(String s) {
        if(s.lastIndexOf(".") != -1 && s.lastIndexOf(".") != 0)
        return s.substring(s.lastIndexOf(".")+1);
        else return "";
    }
	
	/**
	 * Ridimensiona un {@link Image} secondo le dimensioni passate. 
	 * L'immagine viene ridimensionata usando {@link BufferedImage#TYPE_INT_ARGB} di default
	 * 
	 * @param im Immagine da ridimensionare
	 * @param w Larghezza da ottenere
	 * @param h Altezza da ottenere
	 * @return {@link Image} ridimensionata secondo larghezza e altezza passate
	 */
	public static Image getScaledImage(Image im, int w, int h){
	    BufferedImage resizedImg = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
	    Graphics2D g2 = resizedImg.createGraphics();

	    g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
	    g2.drawImage(im, 0, 0, w, h, null);
	    g2.dispose();

	    return resizedImg;
	}
	
	/**
	 * Ridimensione un {@link BufferedImage} secondo le dimensione passate. 
	 * Si consiglia l'utilizzo di questo metodo rispetto a {@link #getScaledImage(Image, int, int)} perch� permette di scegiere il tipo per la nuova immagine 
	 * @param originalImage {@link BufferedImage} da ridimensionare
	 * @param type tipo per la nuova foto. Deve essere supportato da {@link BufferedImage}
	 * @param w Larghezza della foto in uscita
	 * @param h Altezza della foto in uscita
	 * @return {@link BufferedImage} ridimensionata
	 * @see BufferedImage
	 */
	public static BufferedImage resizeImageWithHint(BufferedImage originalImage, int type, int w, int h){
		
		BufferedImage resizedImage = new BufferedImage(w, h, type);
		Graphics2D g = resizedImage.createGraphics();
		g.drawImage(originalImage, 0, 0, w, h, null);
		g.dispose();	
		g.setComposite(AlphaComposite.Src);

		g.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
		RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		g.setRenderingHint(RenderingHints.KEY_RENDERING,
		RenderingHints.VALUE_RENDER_QUALITY);
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
		RenderingHints.VALUE_ANTIALIAS_ON);
		
		return resizedImage;
	    }	
	
	
	/**
	 * Ridimensiona la dimensione mantenendo l'aspect ratio della dimensione iniziale. 
	 * La dimensione viene modificata in base al parametro boundary
	 * @param imgSize dimensione iniziale, su cui viene calcolato l'aspect ratio
	 * @param boundary dimensione su cui calcolare la nuova dimensione
	 * @return nuova {@link Dimension} con lo stesso aspect ratio
	 * @see Dimension
	 */
	public static Dimension getScaledDimension(Dimension imgSize, Dimension boundary) {

	    int original_width = imgSize.width;
	    int original_height = imgSize.height;
	    int bound_width = boundary.width;
	    int bound_height = boundary.height;
	    int new_width = original_width;
	    int new_height = original_height;
	    
	    // controllo se dobbiamo scalare al larghezza
	    if (original_width > bound_width) {
	    	//la scalo la larghezza
	        new_width = bound_width;
	        // scalo l'altezza per mantenere aspect ratio
	        new_height = (new_width * original_height) / original_width;
	    }

	    // controllo se devo tornare a scalare con la nuova altezza
	    if (new_height > bound_height) {
	        //scalo altezza
	        new_height = bound_height;
	        //scalo larghezza per mantenere aspect ratio
	        new_width = (new_height * original_width) / original_height;
	    }

	    return new Dimension(new_width, new_height);
	}
	
	
	/**
	 * Metodo che salva il {@link File} nella posizione corretta.
	 * @param f {@link File} da salvare
	 * @see File
	 */
	public static void save(File f)
	{
		Path source = Paths.get(f.getAbsolutePath());
		System.out.println("provo a salvare il file: " + source);
		FileOutputStream out = null;
		try {
			out = new FileOutputStream("resources"+FileSystems.getDefault().getSeparator()+"foto"+FileSystems.getDefault().getSeparator()+f.getName());
		} catch (FileNotFoundException e1) {
			try {
				out = new FileOutputStream("src"+FileSystems.getDefault().getSeparator()+"resources"+FileSystems.getDefault().getSeparator()+"foto"+FileSystems.getDefault().getSeparator()+f.getName());
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		try {
			Files.copy(source, out);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	/**
	 * Salva il file corrispondente alla stringa relativa ad un URL, e lo salva rinominandolo come la stringa file
	 * @param imageurl Stringa corrispende a {@link URL} da cui recuperare il file da salvare
	 * @param file Nome del file che si vuole salvare
	 * @throws MalformedURLException se stringa url � sbagliata
	 */
	public static void savefotoURL(String imageurl, String file) throws MalformedURLException{
		URL url;
		InputStream is = null;
	    OutputStream os = null;
	    try {
			os = new FileOutputStream("resources"+FileSystems.getDefault().getSeparator()+"foto" + FileSystems.getDefault().getSeparator() + file);
		} catch (FileNotFoundException e1) {
			try {
				os = new FileOutputStream("src"+FileSystems.getDefault().getSeparator()+"resources"+FileSystems.getDefault().getSeparator()+"foto"+FileSystems.getDefault().getSeparator()+file);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} 
		
			url = new URL(imageurl);
			try {
				is = url.openStream();
				
			    byte[] b = new byte[2048];
			    int length;

			    while ((length = is.read(b)) != -1) {
			        os.write(b, 0, length);
			    }
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		    
		    
		
	}
	
	/**
	 * Controlla che file corrispondete a stringa relativa ad url sia effettivamente un immagine
	 * @param url stringa da cui prendere immagine
	 * @return true se file � effettivamente un'immagine
	 */
	public static boolean isImageUrl(String url) {
		Image image;
		try {
			image = ImageIO.read(new URL(url));
			if(image.getWidth(null) == -1){
		         return false;
		    }
		    else{
		         return true;
		    }   
		} catch (IOException e) {
			return false;
		} catch(NullPointerException e) {
			return false;
		}
			
	}
	
	/**
	 * Riodirna i valori del array passato in modo inverso. Esso viene utilizzato prima della rimozione di foto da categorie
	 * @param index Array contenente gli indici delle foto da rimuovere 
	 * @return Array riodinato in modo inverso
	 */
	public static int[] indexReorder(int[] index) {
		int[] newIndex = new int[index.length];
		int j=0;
		for(int i = index.length-1; i>=0;i--) {
			newIndex[j]=index[i];
			j++;
		}
		
		return newIndex;
	}
	
	/**
	 * Controlla che il parametro sia un path valido, ovvero se � possibile assegnare ad un file quel nome
	 * @param path nome da controllare
	 * @return true se il path � corretto, altrimenti false
	 */
	public static boolean isValidPath(String path) {
	    try {
	        Paths.get(path);
	    } catch (InvalidPathException | NullPointerException ex) {
	        return false;
	    }
	    return true;
	}

}
