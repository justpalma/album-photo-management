package src;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Classe per la gestione di un album fotografico, caratterizzato da un nome, una data di creazione, 
 * e una lista di categorie. 
 *
 * @author Alessandro Palmieri
 * 
 */
public class Album implements Serializable {
	
	private static final long serialVersionUID = -2432111931955878786L;
	/**
	 *	{@link ArrayList} di {@link Categoria} 
	 *	@see ArrayList 
	 *	@see Categoria
	 */
	private ArrayList<Categoria> categorie;
	/**
	 * Nome dell'album
	 */
	private String nome;
	/**
	 * {@link Data} di creazione album
	 * @see Data
	 */
	private Data data;
	
	/**
	 * Crea un nuovo album, con un nome, una {@link Data} di creazione e inizializza l'{@link ArrayList} delle categorie
	 * @param nome nome assegnato all'album
	 * @see Data
	 */
	public Album(String nome) {
		this.nome=nome;
		data = new Data();
		categorie = new ArrayList<Categoria>();
	}
	
	/**
	 * Restituisce il nome dell'album
	 * @return Nome dell'album
	 */
	public String getNome() {
		return nome;
	}
	
	/**
	 * Modifica il nome dell'album
	 * @param nome Nuovo nome album
	 */
	
	public void setNome(String nome) {
		this.nome = nome;
	}

	/**
	 * Restituisce {@link ArrayList} di {@link Categoria} 
	 * @return ArrayList delle categorie
	 * @see Categoria
	 */
	public ArrayList<Categoria> getCategorie() {
		return categorie;
	}

	/**
	 * Restituisce la {@link Data} di creazione
	 * @return Data creazione album
	 * @see Data
	 */
	public Data getData() {
		return data;
	}
	/**
	 * Aggiunge una {@link Categoria} all'album non protetta da password
	 * @param t Titolo della Categoria
	 * @param d Descrizone della Categoria
	 * @see Categoria
	 */
	public void addCategoria(String t, String d) {
		categorie.add(new Categoria(t,d));
	}
	/**
	 * Aggiunge una {@link CategoriaPwd} all' album protetta da password
	 * @param t Titolo della Categoria
	 * @param d Descrizone della Categoria
	 * @param pwd Password della Categoria
	 * @see CategoriaPwd
	 */
	public void addCategoria(String t, String d, String pwd) {
		char[] newpwd=pwd.toCharArray();
		categorie.add(new CategoriaPwd(t,d,newpwd));
	}
	
	/**
	 * Restituisce la dimensione dell'album, ovvero il numero delle categorie presenti
	 * @return numero di categorie presenti nell'album
	 */
	public int getSizeAlbum() {
		return categorie.size();
	}
	
	/**
	 * Restituisce l'indice relativo alla lista di categorie della categoria passata come parametro
	 * @param cat Categoria da cercare
	 * @return indice categoria  
	 */
	public int getIndex(Categoria cat) {
		for(int i=0;i<getSizeAlbum();i++) {
			if(categorie.get(i).equals(cat))
				return i;
		}
		return -1;
	}
	/**
	 * Restituisce il nome della categorie all'indice passato
	 * @param n indice della categoria
	 * @return Nome categoria
	 */
	public String getNomeCat(int n) {
		return categorie.get(n).getTitolo();
	}
	
	/**
	 * Restitusce la {@link Categoria} relativa all'indice
	 * @param index indice da cercare
	 * @return Categoria 
	 * @see Categoria
	 */
	public Categoria getCategoria(int index) {
		return categorie.get(index);
	}
	
	/**
	 * Rimuove una categoria dall'album
	 * @param cat {@link Categoria} da rimuovere
	 * @see Categoria
	 */
	public void removeCat(Categoria cat) {
		System.out.println("Rimuovo Categira: "+ cat.getTitolo());
		categorie.remove(cat);
	}
	
	/**
	 * Restituisce il numero di foto presenti nell'album
	 * @return numero foto totali
	 */
	public int getNumberFoto() {
		int nfoto=0;
		for(int i=0;i<this.getSizeAlbum();i++) {
			nfoto+=this.getCategoria(i).getSizeCategoria();
		}
		return nfoto;
	}
	
	/**
	 * Controlla se � gi� presente nell'album una categoria con lo stesso nome
	 * @param name nome Categoria
	 * @return true se � gi� presente, altrimenti false
	 */
	public boolean CategoriaAlreadyEx(String name) {
		for(int i=0;i<this.getSizeAlbum();i++) {
			if(this.getCategoria(i).getTitolo().equals(name))
				return true;
		}
		return false;
	}
	
	
	/**
	 * Unisce due categorie. Le foto presenti nella Categoria "remove" vengono aggiunte in coda alla Categoria "keep"
	 * @param keep	Categoria destinazione in cui verranno aggiunte foto
	 * @param remove Categoria sorgente da cui verranno prese le foto
	 */
	public void mergeCategorie(Categoria keep, Categoria remove) {
		keep.getImm().addAll(remove.getImm());	//aggiungo tutte le foto di remove a keep
		keep.getNomiFoto().addAll(remove.getNomiFoto()); //aggiungo tutti i nomi foto di remove a keep
	}

	/**
	 * Rimuove dall'album la categoria all'indice scelto
	 * @param indexrem indice categoria da rimuovere
	 */
	public void removeCatIndex(int indexrem) {
		categorie.remove(indexrem);
	}
	
	/**
	 * Controlla se nell'album esistono solo Categoria protette da password
	 * @return true se tutte le categorie dell'album sono protette da password, altrimenti false
	 */
	public boolean onlyCatPwd() {
		for(int i=0;i<this.getSizeAlbum();i++) {
			if(this.getCategoria(i) instanceof CategoriaPwd)
				continue;
			else {
				return false;
			}
		}
		return true;
	}
}
