package src;
import java.io.Serializable;
import java.util.Calendar;
import java.util.TimeZone;

/**
 * classe che gestisce la creazione di una data
 * @author Alessandro Palmieri
 * @version 1.0.0
 *
 */

public class Data implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5512338603753498080L;
	/**
	 * attributo per il giorno
	 */
	private int day;
	/**
	 * attributo per il mese
	 */
	private int month;
	/**
	 * attributo per l'anno
	 */
	private int year;
	/**
	 * attributo per le ore
	 */
	private int hh;
	/**
	 * attributo per i minuti
	 */
	private int mm;
	/**
	 * attributo per i secondi
	 */
	private int ss;
	
	
	/**
	 * costruttore che non richiede parametri e inizializza tutti gli attributi della classe
	 */
	public Data() {
		Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
		day = calendar.get(Calendar.DATE);
		month = calendar.get(Calendar.MONTH) + 1;
		year = calendar.get(Calendar.YEAR);
		hh = calendar.get(Calendar.HOUR_OF_DAY);
		mm = calendar.get(Calendar.MINUTE);
		ss = calendar.get(Calendar.SECOND);
	}
	
	/**
	 * crea una stringa con un formato DD/MM/YY hh:mm:ss 
	 * @return stringa appena creata 
	 */
	protected String formatData() {
		String str = new String();
		str = String.format("%02d", getDay()) + "/" + String.format("%02d", getMonth()) + "/" + String.format("%04d", getYear()) + 
				" " + String.format("%02d", getHh()) + ":" + String.format("%02d", getMm()) + ":" + String.format("%02d", getSs());
		return str;
	}
	
	/**
	 * restituisce attributo day
	 * @return attributo day
	 */
	public int getDay() {
		return day;
	}

	/**
	 * restituisce attributo month
	 * @return attributo month
	 */
	public int getMonth() {
		return month;
	}
	
	/**
	 * restituisce attributo year
	 * @return attributo year
	 */
	public int getYear() {
		return year;
	}
	
	/**
	 * restituisce attributo hh
	 * @return attributo hh
	 */
	public int getHh() {
		return hh;
	}
	
	/**
	 * restituisce attributo mm
	 * @return attributo mm
	 */
	public int getMm() {
		return mm;
	}

	/**
	 * restituisce attributo ss
	 * @return attributo ss
	 */
	public int getSs() {
		return ss;
	}
	
	
}
