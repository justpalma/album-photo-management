package src;

import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.FileSystems;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;

	/**
	 * Classe per la Gestione di una Categoria protetta da password
	 * @author Alessandro Palmieri
	 *
	 */

public class CategoriaPwd extends Categoria implements Serializable {
	
	
	private static final long serialVersionUID = 1099950376080145246L;
	/**
	 * Array di char in cui memorizzare la password
	 */
	private char[] pwd;
	
	/**
	 * Inizializza una Categoria protetta da una password
	 * @param titolo titolo della categoria
	 * @param descrizione descrizione della categoria
	 * @param pwd password della categoria
	 */
	public CategoriaPwd(String titolo, String descrizione, char[] pwd) {
		super(titolo,descrizione);
		this.pwd=pwd;
	}
	
	/**
	 * Restituisce la password
	 * @return password in array di char
	 */
	public char[] getPwd() {
		return pwd;
	}
	
	/**
	 * Restituisce la password sottoforma di stringa
	 * @return password
	 */
	public String getPwdString() {
		return new String(pwd);
	}
	

	@Override
	public void setDefaultImage(JButton c) {
		BufferedImage im2;
		String prova = null;
		File folder = null;
		try {
			prova=new File("resources"+FileSystems.getDefault().getSeparator()+"systemfoto").getCanonicalPath();		//eseguito da linea di comando
		} catch (IOException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		folder=new File(prova);
		if(folder.isDirectory()==false) {
			try {
				//da eclipse
				prova = new File("src"+FileSystems.getDefault().getSeparator()+"resources"+FileSystems.getDefault().getSeparator()+"systemfoto").getCanonicalPath();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		try {
			im2 = ImageIO.read(new File(prova+ FileSystems.getDefault().getSeparator() + "password.jpg"));
			int type = im2.getType() == 0? BufferedImage.TYPE_INT_ARGB : im2.getType();
			Dimension dim = Gfoto.getScaledDimension(new Dimension(im2.getWidth(),im2.getHeight()), c.getSize());
			im2 = Gfoto.resizeImageWithHint(im2, type, dim.width, dim.height);
			ImageIcon iii = new ImageIcon(im2);
			c.setIcon(iii);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
