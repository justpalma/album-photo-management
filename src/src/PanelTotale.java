package src;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Rectangle;
import java.util.ArrayList;

import javax.swing.JPanel;
	
	/**
	 * {@link JPanel} personalizzato per la visualizzazione dell'album
	 * @author apalm
	 * @see JPanel
	 */
public class PanelTotale extends JPanel{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7935362240632340304L;
	/**
	 * numero di colonne per la visulizzazione album
	 */
	final int col = 3;
	
	/**
	 * inizializza il pannello
	 * @param pcat {@link ArrayList} di {@link PanelCategorie} da visualizzare nel pannello totale
	 * @see ArrayList 
	 * @see PanelCategorie
	 */
	public PanelTotale(@SuppressWarnings("rawtypes") ArrayList<PanelCategorie> pcat) {
		super();
		setLayout(new GridLayout(0,col));
		for(int i=0;i<pcat.size();i++) {
			add(pcat.get(i));
			pcat.get(i).setOpaque(false);
		}
		this.setBackground(new Color(215, 189, 226));
		
	}
	@Override
	public void paintComponent(Graphics g) {
        g.setColor(new Color(215, 189, 226));
        Rectangle r = g.getClipBounds();
        g.fillRect(r.x, r.y, r.width, r.height);
        super.paintComponent(g);
    }
		
	
}
