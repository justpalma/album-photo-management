package src;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;

import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.FileSystems;
import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;


	/**
	 * Classe per la gestione della guida a schermo per l'utente
	 * @author Palmieri Alessandro
	 *
	 */

public class Guida extends JFrame implements ActionListener, MouseMotionListener{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7188106810583806926L;
	/**
	 * {@link JButton} relativi a tutte le possibile azioni
	 * @see JButton
	 */
	private JButton[] azioni=new JButton[9];
	
	/**
	 * {@link JTextArea} in cui viene visualizzato il contenuto dei file di sistema dove sono spiegate tutte le funzionalitą
	 * @see JTextArea
	 */
	private JTextArea area;
	/**
	 * Stringa in cui viene salvato il percorso della cartella da cui vengono caricatati i file di sistema
	 */
	private String prova;
	
	/**
	 * Inizializza la guida
	 */
	public Guida() {
		super();
		//this.setLayout(null);
		this.setPreferredSize(new Dimension(830,600));
		this.setSize(new Dimension(830,600));
		this.setMaximumSize(new Dimension(830,600));
		this.setTitle("Guida");
		prova = null;
		try {
			 prova = new File("resources"+FileSystems.getDefault().getSeparator()).getCanonicalPath();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		if(new File(prova).isDirectory()==false) {
			prova = new String("src"+FileSystems.getDefault().getSeparator()+"resources");
		}
		
		JPanel ptot = new JPanel(null) {
			/**
			 * 
			 */
			private static final long serialVersionUID = -65419112726452023L;

			@Override protected void paintComponent(Graphics g) {
				super.paintComponent(g);
				try {
					BufferedImage img = ImageIO.read(new File(prova+FileSystems.getDefault().getSeparator()+"systemfoto"+FileSystems.getDefault().getSeparator()+"guida.jpg"));
					g.drawImage(img, 0, 0, this);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
		};
		JPanel ptitolo = new JPanel();
		JPanel pazioni = new JPanel();
		JPanel pdesc = new JPanel(new BorderLayout());
		JLabel titolo = new JLabel("Funzionalitą Album Foto");
		ptitolo.addMouseMotionListener(this);
		area = new JTextArea();
		area.setLineWrap(true);
		area.setEditable(false);
		Font f = new Font(Font.MONOSPACED, Font.BOLD, 20);
		area.setFont(f);
		area.setOpaque(false);
		area.setForeground(Color.WHITE);
		area.addMouseMotionListener(this);
		
		titolo.setForeground(Color.ORANGE);
		titolo.setFont(f);
		ptitolo.add(titolo);
		area.setText("Premere uno dei bottoni per avere la spiegazione\ndella funzionalitą ad esso collegata");
		pdesc.add(area);
		JScrollPane pdescrizione = new JScrollPane(pdesc, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		pdescrizione.setBorder(BorderFactory.createEmptyBorder());
		pazioni.setLayout(new BoxLayout(pazioni,BoxLayout.Y_AXIS));
		pdescrizione.getVerticalScrollBar().setUnitIncrement(10);
		
		pdescrizione.getViewport().setOpaque(false);
		ptitolo.setOpaque(false);
		pazioni.setOpaque(false);
		pdesc.setOpaque(false);
		pdescrizione.setOpaque(false);
		
		ptot.add(ptitolo);
		ptot.add(pazioni);
		ptot.add(pdescrizione);
		
		Insets insets = ptot.getInsets();
		
		ptitolo.setBounds(0+insets.left, 0+insets.top, 782, 50);
		pazioni.setBounds(0+insets.left, 51+insets.top,150,513);
		pdescrizione.setBounds(151+insets.left,51+insets.top,650,510);		
		pazioni.setLayout(new BoxLayout(pazioni,BoxLayout.Y_AXIS));
		
		for(int i=0;i<9;i++) {
			
			azioni[i] = new JButton();
			azioni[i].setFocusPainted(false);
			azioni[i].addMouseMotionListener(this);
			azioni[i].setForeground(Color.WHITE);
			azioni[i].setBorder(BorderFactory.createEmptyBorder());
			azioni[i].setAlignmentX(Component.CENTER_ALIGNMENT);
			azioni[i].setPreferredSize(new Dimension(150,90));
			azioni[i].setMaximumSize(new Dimension(150,90));
			azioni[i].setSize(new Dimension(150,90));
			azioni[i].addActionListener(this);
			azioni[i].setOpaque(false);
			azioni[i].setBackground(Color.LIGHT_GRAY);
			pazioni.add(azioni[i]);
		}
		
		azioni[0].setText("Informazioni Album");
		azioni[1].setText("Modifica Album");
		azioni[2].setText("Modifica Categoria");
		azioni[3].setText("Crea Categoria");
		azioni[4].setText("Rimuovi Categoria");
		azioni[5].setText("Unisci Categorie");
		azioni[6].setText("Aggiungi Foto");
		azioni[7].setText("Rimuovi Foto");
		azioni[8].setText("Avvia Slideshow");
		
		this.add(ptot);
		this.pack();
		this.setResizable(false);
		this.setVisible(true);
		this.setLocationRelativeTo(null);
		
	}

	@Override
	public void actionPerformed(ActionEvent ae) {
		
			FileInputStream read;
			try {
				read = new FileInputStream(prova+FileSystems.getDefault().getSeparator()+"systemfile"+FileSystems.getDefault().getSeparator()+ae.getActionCommand()+".txt");
				BufferedReader br = new BufferedReader(new InputStreamReader(read,"Cp1252"));
				area.read(br, null);
				br.close();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
		
		
	}

	@Override
	public void mouseDragged(MouseEvent arg0) {
		// TODO Auto-generated method stubł
		
	}

	@Override
	public void mouseMoved(MouseEvent me) {
		int val = -1;
		if(me.getSource() instanceof JButton) {
			for(int i=0;i<9;i++) {
				if(me.getSource()==azioni[i]) {
					azioni[i].setBorder(BorderFactory.createLineBorder(Color.WHITE));
					val=i;
					break;
				}
			}
		}
		for(int i=0;i<9;i++) {
			if(i==val)
				continue;
			else
				setBorderEmpty(i);
			}
	}
	
	private void setBorderEmpty(int num) {
		azioni[num].setBorder(BorderFactory.createEmptyBorder());
	}
}
