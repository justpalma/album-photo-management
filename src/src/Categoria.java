package src;

import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.FileSystems;
import java.util.ArrayList;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;

/**
 * Classe per la gestione di una Categoria non protetta da password
 * @author Alessandro Palmieri
 *
 */

public class Categoria implements Serializable{
	
	private static final long serialVersionUID = 9035665980793416127L;
	/**
	 * Titolo della categoria
	 */
	private String titolo;
	/**
	 * Descrizione della categoria
	 */
	private String descrizione;
	/**
	 * Data creazione della categoria
	 */
	private Data data;
	/**
	 * Lista di {@link BufferedImage}
	 * @see BufferedImage
	 */
	private transient ArrayList<BufferedImage> imm;
	/**
	 * Lista dei nomi delle foto presenti nella categoria
	 */
	private ArrayList<String> nomefoto;
	
	/**
	 * Crea una nuova Categoria 
	 * @param titolo titolo della categoria
	 * @param descrizione descrizione della categoria
	 */
	public Categoria(String titolo, String descrizione) {
		
		this.titolo=titolo;
		this.descrizione=descrizione;
		data = new Data();
		imm = new ArrayList<BufferedImage>();
		nomefoto = new ArrayList<String>();
	}
	
	/**
	 * Restituisce il nome della Categoria
	 * @return {@link Categoria#titolo}
	 */
	public String getTitolo() {
		return titolo;
	}
	
	/**
	 * Imposta il titolo della Categoria 
	 * @param titolo Nuovo titolo della categoria
	 */
	public void setTitolo(String titolo) {
		this.titolo = titolo;
	}

	/**
	 * Restituisce la descrizione della Categoria
	 * @return {@link Categoria#descrizione}
	 */
	public String getDescrizione() {
		return descrizione;
	}
	
	/**
	 * Modifica la descrizione della categoria
	 * @param descrizone nuova descrizione della categoria
	 */
	public void setDescrizione(String descrizone) {
		this.descrizione = descrizone;
	}
	
	/**
	 * Restituisce la Data di creazione della categoria
	 * @return {@link Data}
	 * @see Data
	 */
	public Data getData() {
		return data;
	}

	/**
	 * Restituisce il numero di foto presenti nella categoria
	 * @return dimensione categoria 
	 */
	public int getSizeCategoria() {
		return imm.size();
	}
	
	/**
	 * Restituisce {@link BufferedImage}  all' indice passato
	 * @param index indice 
	 * @return Immagine all'indice richiesto
	 * @see BufferedImage
	 */
	public BufferedImage getImage(int index) {
		return imm.get(index);
	}
	
	/**
	 * Restituisce {@link #nomefoto}
	 * @return {@link #nomefoto}
	 */
	public ArrayList<String> getNomiFoto(){
		return nomefoto;
	}
	
	/**
	 * Imposta l'immagine di default nell'anteprima della Categoria
	 * @param c {@link JButton} utilizzato come anteprima Categoria
	 * @see JButton
	 */
	public void setDefaultImage(JButton c) {
		BufferedImage im2;
		String prova = null;
		File folder = null;
		try {
			prova=new File("resources"+FileSystems.getDefault().getSeparator()+"systemfoto").getCanonicalPath();		//eseguito da linea di comando
		} catch (IOException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		folder=new File(prova);
		if(folder.isDirectory()==false) {
			try {
				//da eclipse
				prova = new File("src"+FileSystems.getDefault().getSeparator()+"resources"+FileSystems.getDefault().getSeparator()+"systemfoto").getCanonicalPath();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		try {
			im2 = ImageIO.read(new File(prova+ FileSystems.getDefault().getSeparator() + "empty.png"));
			int type = im2.getType() == 0? BufferedImage.TYPE_INT_ARGB : im2.getType();
			Dimension dim = Gfoto.getScaledDimension(new Dimension(im2.getWidth(),im2.getHeight()), c.getSize());
			im2 = Gfoto.resizeImageWithHint(im2, type, dim.width, dim.height);
			ImageIcon iii = new ImageIcon(im2);
			c.setIcon(iii);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Ricrea ArrayList di immagini della categoria utilizzando i nomi delle foto presenti in {@link #nomefoto}
	 * Questo metodo viene utilizzato ogni volta che apro l'album in modo da avere sempre l'album aggiornato
	 */
	public void recreateIm() {
		imm = new ArrayList<BufferedImage>();
		String prova =null;
		File folder=null;
		try {
			prova=new File("resources"+FileSystems.getDefault().getSeparator()+"foto").getCanonicalPath();		//eseguito da linea di comando
		} catch (IOException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		folder=new File(prova);
		if(folder.isDirectory()==false) {
			try {
				//da eclipse
				prova = new File("src"+FileSystems.getDefault().getSeparator()+"resources"+FileSystems.getDefault().getSeparator()+"foto").getCanonicalPath();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			folder=new File(prova);
		}
		System.out.println("recerate images "+ this.getTitolo());
		for(int i=0;i<nomefoto.size();i++) {
			String file;
			file=prova+FileSystems.getDefault().getSeparator()+nomefoto.get(i);
			File f = new File(file);
			try {
				BufferedImage im = ImageIO.read(f);
				imm.add(im);
			} catch (IOException e) {
				System.out.println("Non ho trovato la foto: " + f.getName()+", essa non si trova pi� nella cartella foto");
				System.out.println("Essa verr� rimossa dalla categoria, si prega di riaggiungerla");
				nomefoto.remove(f.getName());
				
			}
			
			
		}
	}
	
	/**
	 * Aggiugne una {@link BufferedImage} alla categoria
	 * @param im immagine da aggiugnere
	 * @see BufferedImage
	 */
	public void addImage(BufferedImage im) {
		imm.add(im);
	}
	
	/**
	 * Aggiugne in nome del {@link File} alla lista {@link #nomefoto} 
	 * @param f file da aggiungere
	 * @see File
	 */
	public void addStringImage(File f) {
		nomefoto.add(f.getName());
	}
	
	/**
	 * Aggiugne la stringa  alla lista {@link #nomefoto}
	 * @param name nome file da aggiungere
	 */
	public void addNameImage(String name) {
		nomefoto.add(name);
	}
	
	/**
	 * Restituisce il nome della foto all'indice richiesto
	 * @param i indice
	 * @return nome foto
	 */
	public String getNomeFoto(int i) {
		return nomefoto.get(i);
	}

	@Override
	public String toString() {
		return "Categoria [titolo=" + titolo + ", data=" + data + "]";
	}
	
	/**
	 * Rimuove le immagini dalla categoria, il vettore passato corrisponde agli indici delle foto da rimuovere.
	 * Esso deve essere riodinato in modo inverso. Viene utilizzato {@link Gfoto#indexReorder(int[])} per riordinare gli indici
	 * @param delete Vettore di indici delle foto da rimuovere
	 */
	public void removefoto(int[] delete) {
		int indexremove=0;
		for(indexremove=0; indexremove<delete.length;indexremove++) {
			nomefoto.remove(delete[indexremove]);
			imm.remove(delete[indexremove]);
		}
		
	}
	
	/**
	 * Restituisce {@link #imm}
	 * @return {@link #imm}
	 */
	public ArrayList<BufferedImage> getImm() {
		return imm;
	}

	/**
	 * Controlla se la categoria ha gi� una foto con lo stesso nome di quello passato
	 * @param name nome foto da controllare
	 * @return vero se la categoria contiene gi� una foto con lo stesso nome
	 */
	public boolean hasPhoto(String name) {
		if(nomefoto.contains(name))
			return true;
		return false;
		
	}	
}
