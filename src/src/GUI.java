package src;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Random;
import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileSystemView;
import javax.swing.text.MaskFormatter;
import javax.swing.text.NumberFormatter;

import java.io.*;
import java.net.URL;
import java.nio.file.FileSystems;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;

	/**
	 * Classe che gestisce l'interfaccia grafica dell'album. Essa � il corpo principale del software.
	 * Implementa {@link ActionListener} per gestire tutte le possibili operazioni.
	 * @author Palmieri Alessandro
	 * @see ActionListener
	 *
	 */
public class GUI implements ActionListener {

	/**
	 * {@link JFrame} in cui viene visualizzato l'album
	 * @see JFrame
	 */
	private JFrame frame;
	/**
	 * {@link Album} da visualizzare nella GUI
	 * @see Album
	 */
	private Album ab;
	/**
	 * {@link ArrayList} di {@link JButton} utilizzati per l'anteprima delle categorie
	 * @see ArrayList
	 * @see JButton
	 */
	private ArrayList<JButton> cat;				//arraylist di bottoni per visualizzare categorie
	@SuppressWarnings("rawtypes")
	/**
	 * {@link ArrayList} di {@link PanelCategorie} utilizzati per le categorie
	 * @see ArrayList
	 * @see PanelCategorie
	 */
	private ArrayList<PanelCategorie> pcat;
	/**
	 * {@link PanelTotale} in cui vengono attaccati tutti i {@link JComponent}
	 * @see PanelTotale
	 * @see JComponent
	 */
	private PanelTotale ptot;
	
	/**
	 * Inizilizza tutti i {@link JComponent}, {@link #pcat}, {@link #cat}
	 * @param album Album da visualizzare
	 * @see JComponent
	 * @see #pcat
	 * @see #cat
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public GUI(Album album) {
		
		this.ab=album;
		frame = new JFrame(ab.getNome());
		JMenuBar menubar;
		JMenu mfile, mmodifica, mcategoria, mfoto, mslideshow, help;
		JPanel temp = new JPanel(new BorderLayout());	 //pannello totale frame
		
		cat = new ArrayList<JButton>();
		pcat = new ArrayList<PanelCategorie>();
		for(int i=0;i<ab.getSizeAlbum();i++)
		{
			/*
			 * creo bottone dove visualizzare anteprima
			 * imposto il nome del bottone come il titolo della categoria
			 * creo panello per la visualizzazione delle anteprime categoria e lo aggiungo all'arraylist di pannelli
			 */
			
			cat.add(new JButton());	
			cat.get(i).setBackground(Color.white);
			cat.get(i).setName(ab.getNomeCat(i));	
			pcat.add(new PanelCategorie(cat.get(i),ab.getCategoria(i),frame));	
			Random rand = new Random();
			if(ab.getCategoria(i) instanceof CategoriaPwd || ab.getCategoria(i).getSizeCategoria()==0) 
				ab.getCategoria(i).setDefaultImage(cat.get(i));
			else
			{
				/*
				 * Calcolo un numero random tra 0 e la dimensione della categoria
				 * prendo la foto di indice n, la ridimensiono e la imposto come anteprima 
				 */
				System.out.println("Lavoro sulla categoria: " + ab.getCategoria(i).getTitolo());
				int n = rand.nextInt(ab.getCategoria(i).getSizeCategoria());
				BufferedImage buff = ab.getCategoria(i).getImage(n);
				int type = buff.getType() == 0? BufferedImage.TYPE_INT_ARGB : buff.getType();
				Dimension dim = Gfoto.getScaledDimension(new Dimension(buff.getWidth(),buff.getHeight()), cat.get(i).getSize());
				buff = Gfoto.resizeImageWithHint(buff, type, dim.width, dim.height);
				ImageIcon i2 = new ImageIcon(buff);
				cat.get(i).setIcon(i2);				
			}
		}
		
		//creo il pannello totale, passandolgi arraylist di pannelli anteprime categorie
		ptot = new PanelTotale(pcat);	
		
		//Trasformo il pannello totale in in pannello scollabile
		JScrollPane scrollPane = new JScrollPane(ptot);
	
		
		//le scrollbar verticali e orizzontali si visualizzano solo se necessarie
        scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        scrollPane.getVerticalScrollBar().setUnitIncrement(10);  //per settare scroll da rotella mouse
        
        //creo barra per il menu e il sottomenu "menu"
		menubar = new JMenuBar();
		
		mfile = new JMenu("File");
		mmodifica = new JMenu("Modifica");
		mcategoria = new JMenu("Gestione Categoria");
		mfoto = new JMenu("Gestione Foto");
		mslideshow = new JMenu("Slideshow");
		help = new JMenu("Help");
		
		
		
		//menu "File"
		JMenuItem mi = new JMenuItem("Informazioni Album");
		mfile.add(mi);
		mi.addActionListener(this);
	
		mi = new JMenuItem("Esci Senza Salvare");
		mfile.add(mi);
		mi.addActionListener(this);
	
		//menu "Modifica"
		mi = new JMenuItem("Modifica Album");
		mmodifica.add(mi);
		mi.addActionListener(this);
		
		mi = new JMenuItem("Modifica categoria");
		mmodifica.add(mi);
		mi.addActionListener(this);
	 	
		//menu "Categoria"
	 	mi = new JMenuItem("Crea categoria");
		mcategoria.add(mi);
		mi.addActionListener(this); 
		
		mi = new JMenuItem("Rimuovi categoria");
		mcategoria.add(mi);
		mi.addActionListener(this); 
		
		mi = new JMenuItem("Unisci categorie");
		mcategoria.add(mi);
		mi.addActionListener(this);
		
		//menu "foto"
		mi = new JMenuItem("Aggiungi foto");
		mfoto.add(mi);
		mi.addActionListener(this);
		
		mi = new JMenuItem("Aggiungi foto da URL");
		mfoto.add(mi);
		mi.addActionListener(this);
		
		mi = new JMenuItem("Rimuovi foto");
		mfoto.add(mi);
		mi.addActionListener(this);
		
		mi = new JMenuItem("Avvia Slideshow");
		mslideshow.add(mi);
		mi.addActionListener(this);
		
		//menu "help"
		mi = new JMenuItem("Informazioni su AlbumPhoto");
		help.add(mi);
		mi.addActionListener(this);
				
		menubar.add(mfile);
		menubar.add(mmodifica);
		menubar.add(mcategoria);
		menubar.add(mfoto);
		menubar.add(mslideshow);
		menubar.add(help);
		
		temp.add(menubar,BorderLayout.NORTH);
		temp.add(scrollPane,BorderLayout.CENTER);
		
		//attacco al frame il pannello
	 	frame.add(temp);
	 	
	 	
	 	
	 	/*
	 	 * imposto la dimensione del frame alla massima possibile
	 	 * imposto il frame centrato (inutile avendo settato il frame come fullscreen)
	 	 * setto il frame non ridimensionabile
	 	 * setto il frame visibile
	 	 */
	 	frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
	    frame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);  //cosi posso gestire il salvataggio dell'album
	    frame.setPreferredSize(new Dimension(800,500));
	    frame.pack();
		frame.setLocationRelativeTo(null);		//inutile
		frame.setResizable(true);
		frame.setVisible(true);
		
		frame.addWindowListener(new WindowAdapter() 
		{
			@Override
			public void windowClosing(WindowEvent e) 
			{
				String prova = null;
				try {
					prova = new File("resources"+FileSystems.getDefault().getSeparator()+"album").getCanonicalPath();
				} catch (IOException e2) {
					e2.printStackTrace();
				}
				File root = new File(prova);
				if(root.isDirectory()==false) {
					root=new File("src"+FileSystems.getDefault().getSeparator()+"resources"+FileSystems.getDefault().getSeparator()+"album");
				}
				
				FileSystemView fsv = new FileSystemViewSingolaDir(root);		
				JFileChooser fileChooser = new JFileChooser(fsv);			//imposto il filechooser sulla singola cartella "album"
				fileChooser.setSelectedFile(new File(ab.getNome()));
		
				int azione = fileChooser.showSaveDialog(frame);		//filechooser per salvare
				
				if(azione==JFileChooser.APPROVE_OPTION) {
					//salvo il file
					ab.setNome(fileChooser.getSelectedFile().getName());
					ObjectOutputStream os = null;
					FileOutputStream f = null;
					try {
						
						f = new FileOutputStream(root.getCanonicalPath()+FileSystems.getDefault().getSeparator() + ab.getNome() + ".dat");	
						os = new ObjectOutputStream(f);
						os.writeObject(ab);
						os.flush();
						os.close();
						System.out.println("Salvataggio Album in corso");
						System.exit(2);
					} catch (FileNotFoundException e1) {
						e1.printStackTrace();
					}catch (IOException e1) {
						e1.printStackTrace();
					}	
					System.exit(0);	
				}
				
			}	
		});
	}
		

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getActionCommand().equals("Informazioni Album")) {
			PanelInfoAlbum info = new PanelInfoAlbum(ab);
			JFrame finfo = new JFrame("Informazioni Album");
			finfo.add(info);
			finfo.setVisible(true);
			finfo.setLocationRelativeTo(null);
			finfo.pack();
			frame.setEnabled(false);
			//frame.setVisible(false);
			finfo.addWindowListener(new WindowAdapter() 
			{
				@Override
				public void windowClosing(WindowEvent e) 
				{
					frame.setEnabled(true);
					finfo.dispose();
				}
				
			});
			
			
		}//fine info album
		if(e.getActionCommand().equals("Esci Senza Salvare")) {
			int result = JOptionPane.showConfirmDialog(null, "Vuoi uscire senza salvare?", "Attenzione", JOptionPane.YES_NO_OPTION);
			if(result==JOptionPane.YES_OPTION)
				System.exit(9);
		}
		
		if(e.getActionCommand().equals("Informazioni su AlbumPhoto")) {
			System.out.println("Apro la guida");
			frame.setEnabled(false);
			Guida g = new Guida();
			g.addWindowListener(new WindowAdapter() {
				public void windowClosing(WindowEvent e) {
					frame.setEnabled(true);
				}
			});
		}
		if(e.getActionCommand().equals("Modifica Album")) {
			Object[] option= {"OK"};
	    	JTextField string = new JTextField(20);
	    	JPanel panel = new JPanel();
	    	panel.add(new JLabel("Nome Album:"));
	    	panel.add(string);
	    	string.addAncestorListener(new FocusListener());
	    	int ret = JOptionPane.showOptionDialog(null, panel, "Inserisci il nome dell' album", JOptionPane.NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, option,option[0]);
	    	
	    	if(ret==0) {
	    		String str = null;
	    		if(string.getText().equals("")) {
	    			JOptionPane.showMessageDialog(frame, "Non � possibile inserire un nome vuoto");
	    		}
	    		
	    		else if(string.getText().length()>50) {
	    			//nome album maggiore di 50 caratteri, lo riduco a 50
	    			 str = string.getText().substring(0, Math.min(string.getText().length(), 50));
	    			 ab.setNome(str);
	    			 frame.setTitle(str);
	    		}
	    		else {
	    			str = new String(string.getText());
	    			ab.setNome(str);
	    			frame.setTitle(str);
	    		}
			
	    	}
		}//fine modifica album
		if(e.getActionCommand().equals("Modifica categoria")) {
			if(ab.getSizeAlbum()==0) {
				JOptionPane.showMessageDialog(null, "Album senza Categoria, creare prima una categoria");
			}
			else {
			PanelScelta p = new PanelScelta(ab.getCategorie());
			int scelta = JOptionPane.showConfirmDialog(null, p, "Scegli categoria da modificare", JOptionPane.YES_NO_OPTION);
			if(scelta==JOptionPane.OK_OPTION) {
				PanelModificaCat m = new PanelModificaCat();
				m.setPanel(p.getCategoriaList().getTitolo(), p.getCategoriaList().getDescrizione());
				if(p.getCategoriaList() instanceof CategoriaPwd) {
					JPasswordField pwd = new JPasswordField();
					pwd.addAncestorListener(new FocusListener());
					int action = JOptionPane.showConfirmDialog(null, pwd, "Inserisci password", JOptionPane.OK_CANCEL_OPTION);
					if(action==JOptionPane.OK_OPTION) {
						String str = new String(pwd.getPassword());
						CategoriaPwd cat = p.getCategoriaPwdList();
						if(cat.getPwdString().equals(str)) {
							int ris = JOptionPane.showConfirmDialog(null, m, "Modifica la categoria " + m.getName(), JOptionPane.OK_CANCEL_OPTION);
							if(ris==JOptionPane.OK_OPTION) {
								if(m.getName().equals("")) 
									JOptionPane.showMessageDialog(null, "Impossibile modificare una categoria con il nome vuoto");
								else if(ab.CategoriaAlreadyEx(m.getName()) && m.getDescrizione().equals(p.getCategoriaList().getDescrizione()))
									JOptionPane.showMessageDialog(null, "Categoria gi� esistente");
								else {
									p.getCategoriaPwdList().setTitolo(m.getName());
									p.getCategoriaPwdList().setDescrizione(m.getDescrizione());
									int index = ab.getIndex(p.getCategoriaPwdList());
									pcat.get(index).updatePanel(m.getName(), m.getDescrizione());
								}
						}
					}
					else {
						JOptionPane.showMessageDialog(null,
							    "Password Errata",
							    "Errore",
							    JOptionPane.ERROR_MESSAGE);
					}
					}
						
				}//fine categoria con pwd
				else {
					int ris = JOptionPane.showConfirmDialog(null, m, "Modifica la categoria" + m.getName(), JOptionPane.OK_CANCEL_OPTION);
					if(ris==JOptionPane.OK_OPTION) {
						if(m.getName().equals("")) 
							JOptionPane.showMessageDialog(null, "Impossibile modificare una categoria con il nome vuoto");
						else if(ab.CategoriaAlreadyEx(m.getName()) && m.getDescrizione().equals(p.getCategoriaList().getDescrizione()))
							JOptionPane.showMessageDialog(null, "Categoria gi� esistente");
						else {
							p.getCategoriaList().setTitolo(m.getName());
							p.getCategoriaList().setDescrizione(m.getDescrizione());
							int index = ab.getIndex(p.getCategoriaList());
							pcat.get(index).updatePanel(m.getName(), m.getDescrizione());
						}
				}
				}
				
			}
			}
		}//fine modifica categoria
		
		if(e.getActionCommand().equals("Crea categoria")) {
			PanelAggiuntaCategoria p = new PanelAggiuntaCategoria();	//pannello per l'inserimento dei dati relativi alla categoria
			//chiedo all'utente i dati della categoria
			int scelta = JOptionPane.showConfirmDialog(null,p, "Creazione nuova categoria", JOptionPane.OK_CANCEL_OPTION );
			if((scelta==JOptionPane.OK_OPTION) && (!p.getTitle().isEmpty()) && (!ab.CategoriaAlreadyEx(p.getTitle()))) {
				
				String nome = p.getTitle();
				String pwd = p.getPasswordString();
				String desc = p.getDescrizione();
				if(pwd.equals("")) {					//categoria senza password
					ab.addCategoria(nome, desc);
				}
				else {
					ab.addCategoria(nome, desc, pwd);	//categoria con password
				}
				cat.add(new JButton());					//aggiungo bottone relativo alla categoria
				cat.get(ab.getSizeAlbum()-1).setName(nome);		//setto il nome del bottone
				int index = ab.getSizeAlbum()-1;				//calcolo indice nuova categoria
				pcat.add(new PanelCategorie<Categoria>(cat.get(index), ab.getCategoria(index), frame));		//aggiungo il pannello per la categoria nuova
				
				/*	setto l'immagine di default per la categoria
				 * 	non devo controllare se la categoria ha la pwd
				 * 	ho fatto l'override del metodo e sar� il compilatore a capire quale chiamare
				 * 
				 */
				ab.getCategoria(index).setDefaultImage(cat.get(index));	
				ptot.add(pcat.get(ab.getSizeAlbum()-1));		//aggiungo il pannello
				ptot.revalidate();			//revalido il pannello
			}	
			else if((scelta==JOptionPane.OK_OPTION) && p.getTitle().isEmpty()){
				JOptionPane.showMessageDialog(null, "Impossibile creare una categoria senza nome", "ATTENZIONE", JOptionPane.ERROR_MESSAGE);
			}
			else if((scelta==JOptionPane.OK_OPTION) && ab.CategoriaAlreadyEx(p.getTitle())) {
				JOptionPane.showMessageDialog(null, "Impossibile creare una categoria con il nome uguale a quello di una gi� esistente", "ATTENZIONE", JOptionPane.ERROR_MESSAGE);
			}
		}//fine aggiunta categoria
		
		if(e.getActionCommand().equals("Rimuovi categoria")) {
			if(ab.getSizeAlbum()==0){
				JOptionPane.showMessageDialog(null, "Impossibile rimuovere una categoria, l'album � vuoto", "ATTENZIONE", JOptionPane.ERROR_MESSAGE);
			}
			else {
				PanelScelta p = new PanelScelta(ab.getCategorie());
				int scelta = JOptionPane.showConfirmDialog(null, p, "Rimuovi Categoria", JOptionPane.YES_NO_OPTION);
				if(scelta==JOptionPane.OK_OPTION) {
					if(p.getCategoriaList() instanceof CategoriaPwd) {
						JPasswordField pwd = new JPasswordField();
						pwd.addAncestorListener(new FocusListener());
						int action = JOptionPane.showConfirmDialog(null, pwd, "Inserisci password", JOptionPane.OK_CANCEL_OPTION);
						if(action==JOptionPane.OK_OPTION) {
							String str = new String(pwd.getPassword());
							CategoriaPwd cat = p.getCategoriaPwdList();
							if(cat.getPwdString().equals(str)) {
								int i = ab.getIndex(cat);
								ab.removeCat(cat);		//rimuovo categoria dall'album
								pcat.remove(i);			//rimuovo pannello dalla lista
								ptot.remove(i);			//rimuovo pannello
								ptot.revalidate();		//revalido il pannello totale
								ptot.repaint();	
							}
							else {
								JOptionPane.showMessageDialog(null,
										"Password Errata",
										"Errore",
										JOptionPane.ERROR_MESSAGE);
							}
						}
						
					}//fine categoria con pwd
					else {
						Categoria cat = p.getCategoriaList();
						int i = ab.getIndex(cat);
						ab.removeCat(cat);		//rimuovo categoria dall'album
						pcat.remove(i);			//rimuovo pannello dalla lista
						ptot.remove(i);			//rimuovo pannello
						ptot.revalidate();		//revalido il pannello totale
						ptot.repaint();	
					}
				}
			}
			
		}//fine rimuovi categorie
		
		
		if(e.getActionCommand().equals("Unisci categorie")) {

			/*
			 *sceglo categoria sorgente e destinazione
			 *copio categoria sorgente in destinazione
			 *elimino categoria sorgente
			 */
			if(ab.getSizeAlbum()<=1) 
				JOptionPane.showMessageDialog(null, "Sono necessarie almeno due categorie per poterle unire");
			else {
			PanelScelta sorgente = new PanelScelta(ab.getCategorie());		//va elimianta
			PanelScelta destinazione = new PanelScelta(ab.getCategorie());	//ci aggiungo sorgente e la elimino
			int rsorg = JOptionPane.showConfirmDialog(null, sorgente, "Categoria sorgente, ATTENZIONE questa categoria verr� rimossa", JOptionPane.YES_NO_OPTION);
			if(rsorg==JOptionPane.YES_OPTION) {
			int rdest = JOptionPane.showConfirmDialog(null, destinazione, "Categoria destinazione", JOptionPane.YES_NO_OPTION);
			if(rdest==JOptionPane.YES_OPTION && (sorgente.getCategoriaList()!=destinazione.getCategoriaList())) {
				
				boolean sorgpwd=false;
				boolean destpwd=false;
				
				if(destinazione.getCategoriaList() instanceof CategoriaPwd)
				{
					//seconda categoria con pwd
					JPasswordField pwd = new JPasswordField();
					pwd.addAncestorListener(new FocusListener());
					int action = JOptionPane.showConfirmDialog(null, pwd, "Inserisci password della categoria: " + destinazione.getCategoriaPwdList().getTitolo(), JOptionPane.OK_CANCEL_OPTION);
					if(action==JOptionPane.OK_OPTION) {
						String str = new String(pwd.getPassword());
						if(destinazione.getCategoriaPwdList().getPwdString().equals(str)) {
							//password corretta di categoria destinazione
							destpwd=true;
							if(sorgente.getCategoriaList() instanceof CategoriaPwd) {
								//anche qui chiedo pwd
								//categoria sorgente con pwd
								pwd = new JPasswordField();
								pwd.addAncestorListener(new FocusListener());
								action = JOptionPane.showConfirmDialog(null, pwd, "Inserisci password della categoria: " + sorgente.getCategoriaPwdList().getTitolo(), JOptionPane.OK_CANCEL_OPTION);
								if(action==JOptionPane.OK_OPTION) {
									str = new String(pwd.getPassword());
									if(sorgente.getCategoriaPwdList().getPwdString().equals(str)) {
										//anche prima categoria ha pwd
										sorgpwd=true;
									}
									if(destpwd&&sorgpwd) {
										//entrambe le pwd corrette
										// le posso unire
										int index=ab.getIndex(destinazione.getCategoriaPwdList());
										int indexrem=ab.getIndex(sorgente.getCategoriaPwdList());
										ab.mergeCategorie(destinazione.getCategoriaPwdList(), sorgente.getCategoriaPwdList());
										pcat.get(index).updateCategoria();
										cat.remove(indexrem);
										pcat.remove(indexrem);
										ptot.remove(indexrem);
										ab.removeCatIndex(indexrem);
										ptot.revalidate();	
										ptot.repaint();
									}
							}
						}
							else {
								//categoria 1 non ha pwd
								int index=ab.getIndex(destinazione.getCategoriaPwdList());
								int indexrem=ab.getIndex(sorgente.getCategoriaList());
								ab.mergeCategorie(destinazione.getCategoriaPwdList(), sorgente.getCategoriaList());
								pcat.get(index).updateCategoria();
								cat.remove(indexrem);
								pcat.remove(indexrem);
								ptot.remove(indexrem);
								ab.removeCatIndex(indexrem);
								ptot.revalidate();	
								ptot.repaint();
								
							}
						}//fine if cat2 pwd
						else {
							JOptionPane.showMessageDialog(null,
									"Password Errata della categoria " + destinazione.getCategoriaPwdList().getTitolo(),
									"Errore",
									JOptionPane.ERROR_MESSAGE);
						}
					}
					
				}
				else if(sorgente.getCategoriaList() instanceof CategoriaPwd) {
					//categoria sorgente ha pawd
					JPasswordField pwd = new JPasswordField();
					pwd.addAncestorListener(new FocusListener());
					int action = JOptionPane.showConfirmDialog(null, pwd, "Inserisci password della categoria: " + sorgente.getCategoriaPwdList().getTitolo(), JOptionPane.OK_CANCEL_OPTION);
					if(action==JOptionPane.OK_OPTION) {
						String str = new String(pwd.getPassword());
						if(sorgente.getCategoriaPwdList().getPwdString().equals(str)) {
							//password corretta
							int index=ab.getIndex(destinazione.getCategoriaList());
							int indexrem=ab.getIndex(sorgente.getCategoriaPwdList());
							ab.mergeCategorie(destinazione.getCategoriaList(), sorgente.getCategoriaPwdList());
							pcat.get(index).updateCategoria();
							cat.remove(indexrem);
							pcat.remove(indexrem);
							ptot.remove(indexrem);
							ab.removeCatIndex(indexrem);
							ptot.revalidate();	
							ptot.repaint();
						}
						else {
							JOptionPane.showMessageDialog(null,
									"Password Errata della categoria " + sorgente.getCategoriaPwdList().getTitolo(),
									"Errore",
									JOptionPane.ERROR_MESSAGE);
						}
					}
					
				}
				
				else {
					//nessuna categoria ha la pwd, le posso unire
					int index=ab.getIndex(destinazione.getCategoriaList());
					int indexrem=ab.getIndex(sorgente.getCategoriaList());
					ab.mergeCategorie(destinazione.getCategoriaList(), sorgente.getCategoriaList());
					pcat.get(index).updateCategoria();
					cat.remove(indexrem);
					pcat.remove(indexrem);
					ptot.remove(indexrem);
					ab.removeCatIndex(indexrem);
					ptot.revalidate();	
					ptot.repaint();
				}
					
			}//fine ok
			else {
				JOptionPane.showMessageDialog(null,
					    "Hai Scelto la stessa categoria, impossibile unirle",
					    "Errore",
					    JOptionPane.ERROR_MESSAGE);
				
			}
			}
			}
			
		}//fine unisci categorie
		
		if(e.getActionCommand().equals("Aggiungi foto")) {
			if(ab.getSizeAlbum()==0) {
				JOptionPane.showMessageDialog(null,
					    "Album vuoto, Impossibile aggiungere foto",
					    "Errore",
					    JOptionPane.ERROR_MESSAGE);
			}
			else {
				JDialog save = new JDialog();
				JPanel pdialog = new JPanel(new BorderLayout());
				JLabel lbdialog = new JLabel("Sto aggiungendo i file alla categoria");
				pdialog.add(lbdialog, BorderLayout.CENTER);
				save.add(pdialog);
				save.setLocationRelativeTo(null);
				save.pack();
			JFileChooser fc = new JFileChooser();
			String Folder = null;
			try {
				Folder = new File("resources"+FileSystems.getDefault().getSeparator()+"foto").getCanonicalPath();
			} catch (IOException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
			if(new File(Folder).isDirectory()==false) {
				try {
					Folder = new File("src"+FileSystems.getDefault().getSeparator()+"resources"+FileSystems.getDefault().getSeparator()+"foto").getCanonicalPath();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			fc.setCurrentDirectory(new File(Folder));
			fc.setMultiSelectionEnabled(true);
			int ret = fc.showOpenDialog(frame);
			if(ret==JFileChooser.APPROVE_OPTION) {
				save.setVisible(true);
				File[] f = fc.getSelectedFiles();
				PanelScelta p = new PanelScelta(ab.getCategorie());
				int ris = JOptionPane.showConfirmDialog(null, p, "Scegli Categoria in cui aggiungere foto", JOptionPane.YES_NO_OPTION);
				if(ris==JOptionPane.YES_OPTION) {
					if(p.getCategoriaList() instanceof CategoriaPwd) {
						JPasswordField pwd = new JPasswordField();
						pwd.addAncestorListener(new FocusListener());
						int action = JOptionPane.showConfirmDialog(null, pwd, "Inserisci password", JOptionPane.OK_CANCEL_OPTION);
						if(action==JOptionPane.OK_OPTION) {
							String str = new String(pwd.getPassword());	//converto la password in stringa
							if(p.getCategoriaPwdList().getPwdString().equals(str)) {
							//aggiugo foto
							for(int l=0;l<f.length;l++) {
								if(p.getCategoriaPwdList().hasPhoto(f[l].getName()))
									continue;
								System.out.println("LAVORO SUL FILE " + f[l].getName());
								System.out.println("Il file � foto? " + Gfoto.isFoto(f[l]));
								if(Gfoto.isFoto(f[l])){
									if(!f[l].getParentFile().getName().equals("foto")){
										//se la foto non si trova nella directory foto, ce la copio
										//controllo del nome foto, attenzione a non sovrascrivere la foto -> DA AGGIUNGERE
										System.out.println("Sto copiando " + f[l].getName());
										Gfoto.save(f[l]);
									}	
									int i=ab.getIndex(p.getCategoriaPwdList());
									BufferedImage im;
									try {
										im = ImageIO.read(f[l]);
										ab.getCategoria(i).addImage(im);
										ab.getCategoria(i).addStringImage(f[l]);
										System.out.println("HO AGGIUNTO FOTO " + f[l].getName() + " alla categoria " + ab.getCategoria(i).getTitolo() );
										pcat.get(i).updateCategoria();
									} catch (IOException e1) {
										e1.printStackTrace();
									}
									
								} //fine ciclo file � foto
							}//fine for
						}//fine password corretta
						else {
							JOptionPane.showMessageDialog(null,
								    "Password Errata",
								    "Errore",
								    JOptionPane.ERROR_MESSAGE);
						}//fine else
							}//fine ho scelto ok
						
	
					}//fine categorie con pwd
					else {
						for(int l=0;l<f.length;l++) 
						{
							if(p.getCategoriaList().hasPhoto(f[l].getName()))
								continue;
							System.out.println("LAVORO SUL FILE " + f[l].getName());
							System.out.println("Il file � foto? " + Gfoto.isFoto(f[l]));
							if(Gfoto.isFoto(f[l]))
							{
								if(!f[l].getParentFile().getName().equals("foto"))
								{
									
									//se la foto non si trova nella directory foto, ce la copio
									//controllo del nome foto, attenzione a non sovrascrivere la foto -> DA AGGIUNGERE
									System.out.println("Sto copiando " + f[l].getName());
									Gfoto.save(f[l]);
				
								}	
								int i=ab.getIndex(p.getCategoriaList());
								BufferedImage im;
								try 
								{
									im = ImageIO.read(f[l]);
									ab.getCategoria(i).addImage(im);
									ab.getCategoria(i).addStringImage(f[l]);
									System.out.println("HO AGGIUNTO FOTO " + f[l].getName() + " alla categoria " + ab.getCategoria(i).getTitolo() );
									pcat.get(i).updateCategoria();
								} catch (IOException e1) 
								{
									e1.printStackTrace();
								}
							}
						}
					}
				}//fine yes option
			}
			save.dispose();
			}
		}//fine aggiunta foto
		
		if((e.getActionCommand().equals("Aggiungi foto da URL"))) {	
			if(ab.getSizeAlbum()==0)
			{
				if(ab.getSizeAlbum()==0) {
					JOptionPane.showMessageDialog(null,
						    "Album vuoto, Impossibile aggiungere foto",
						    "Errore",
						    JOptionPane.ERROR_MESSAGE);
				}
			}
			else {
				
			
			Object[] option= {"OK"};
	    	JLabel lb = new JLabel("Inserisci URL");
	    	JPanel panel = new JPanel();
	    	panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
	    	JTextField texturl = new JTextField();
	    	panel.add(lb);
	    	panel.add(texturl);
	    	texturl.addAncestorListener(new FocusListener());
	    	int ret = JOptionPane.showOptionDialog(null, panel, "Aggiunta foto da URL", JOptionPane.NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, option,option[0]);
	    	if(ret==0) {
	    		//controllo che url corrisponda a foto
	    		//isfoto controllo che estensione file url sia quello di una foto e che sia supportato
	    		//isimageurl controlla che il file corrispondente all'url sia effettivamente una foto caricabile
				if(Gfoto.isFoto(new File(texturl.getText())))
				{
					if(Gfoto.isImageUrl(texturl.getText())) {
						//sono sicuro che file sia una foto valida
				    	JLabel label = new JLabel("Inserisci Nome File");
				    	JPanel panelnome = new JPanel();
				    	panelnome.setLayout(new BoxLayout(panelnome, BoxLayout.Y_AXIS));
				    	JTextField nome = new JTextField(20);
				    	panelnome.add(label);
				    	panelnome.add(nome);
				    	nome.addAncestorListener(new FocusListener());
				    	int val = JOptionPane.showOptionDialog(null, panelnome, "Aggiunta foto da URL", JOptionPane.NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, option,option[0]);
				    	if(val==0) {
						String name = nome.getText();
						String ext = Gfoto.getExtension(texturl.getText());
						//System.out.println(name+"."+ext);
						if(Gfoto.isValidPath(name))
						{
							//scelgo categoria
							PanelScelta p = new PanelScelta(ab.getCategorie());
							int ris = JOptionPane.showConfirmDialog(null, p, "Scegli Categoria in cui aggiungere foto", JOptionPane.YES_NO_OPTION);
							if(ris==JOptionPane.YES_OPTION) {
								BufferedImage img = null;
								try {
									img = ImageIO.read(new URL(texturl.getText()));
								} catch (IOException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}
								if(p.getCategoriaList() instanceof CategoriaPwd) {
									JPasswordField pwd = new JPasswordField();
									int action = JOptionPane.showConfirmDialog(null, pwd, "Inserisci password", JOptionPane.OK_CANCEL_OPTION);
									if(action==JOptionPane.OK_OPTION) {
										String str = new String(pwd.getPassword());	//converto la password in stringa
										if(p.getCategoriaPwdList().getPwdString().equals(str)) {
										//aggiugo foto
											int index = ab.getIndex(p.getCategoriaPwdList());
											ab.getCategoria(index).addImage(img);
											ab.getCategoria(index).addNameImage(name+"."+ext);
											pcat.get(index).updateCategoria();
										}
										else {
											JOptionPane.showMessageDialog(null,
												    "Password Errata",
												    "Errore",
												    JOptionPane.ERROR_MESSAGE);
										}
									}
								}//fine categoria con pwd
								else {
									int index = ab.getIndex(p.getCategoriaList());
									ab.getCategoria(index).addImage(img);
									ab.getCategoria(index).addNameImage(name+"."+ext);
									pcat.get(index).updateCategoria();
								}
							}
							
							
							try {
								Gfoto.savefotoURL(texturl.getText(), name+"."+ext);
								
							} catch (IOException e1) {
								e1.printStackTrace();
							}
						}
							
						else {
							JOptionPane.showMessageDialog(null,
								    "Nome File non valido",
								    "Errore",
								    JOptionPane.ERROR_MESSAGE);
						}
					}
				}
				else {
					JOptionPane.showMessageDialog(null,
						    "L'immagine non � stata caricata correttamente, � probabile che l'URL inserito sia sbagliato",
						    "Errore",
						    JOptionPane.ERROR_MESSAGE);
				}
				}	
				else {
					JOptionPane.showMessageDialog(null,
						    "L'URL inserito non corrisponde a una foto",
						    "Errore",
						    JOptionPane.ERROR_MESSAGE);
				}
			
		}
			}
		}
		if((e.getActionCommand().equals("Rimuovi foto"))){
			if(ab.getSizeAlbum()!=0) {
			PanelScelta p = new PanelScelta(ab.getCategorie());
			int ris = JOptionPane.showConfirmDialog(null, p,"Scegli categoria da cui rimuovere foto", JOptionPane.YES_NO_OPTION);
			if(ris==JOptionPane.YES_OPTION && p.getCategoriaList().getSizeCategoria()!=0) {
				if(p.getCategoriaList() instanceof CategoriaPwd) {
					JPasswordField pwd = new JPasswordField();
					int action = JOptionPane.showConfirmDialog(null, pwd, "Inserisci password", JOptionPane.OK_CANCEL_OPTION);
					if(action==JOptionPane.OK_OPTION) {
						String str = new String(pwd.getPassword());
						if(p.getCategoriaPwdList().getPwdString().equals(str)) {
							int index = ab.getIndex(p.getCategoriaPwdList());
							PanelRimuoviFoto r = new PanelRimuoviFoto(p.getCategoriaPwdList().getNomiFoto());
							int t = JOptionPane.showConfirmDialog(null, r, "Scegli foto da rimuovere", JOptionPane.YES_NO_OPTION);
							if(t == JOptionPane.YES_OPTION) {
								int[] delete = r.getIndexSelected();
								delete = Gfoto.indexReorder(delete);
								ab.getCategoria(index).removefoto(delete);
								pcat.get(index).updateCategoria();
							}
						}	
					else {
						JOptionPane.showMessageDialog(null,
							    "Password Errata",
							    "Errore",
							    JOptionPane.ERROR_MESSAGE);
						}
						}
					
				}//fine categoria pwd
				else {
					PanelRimuoviFoto r = new PanelRimuoviFoto(p.getCategoriaList().getNomiFoto());
					int t = JOptionPane.showConfirmDialog(null, r, "Scegli foto da rimuovere", JOptionPane.YES_NO_OPTION);
					if(t == JOptionPane.YES_OPTION) {
						int index = ab.getIndex(p.getCategoriaList());
						int[] delete = r.getIndexSelected();
						delete = Gfoto.indexReorder(delete);
						ab.getCategoria(index).removefoto(delete);
						pcat.get(index).updateCategoria();
					}	
				}
			}//fine yes option
			else if(ris==JOptionPane.YES_OPTION && p.getCategoriaList().getSizeCategoria()==0) {
				JOptionPane.showMessageDialog(null,
					    "Categoria Vuota impossibile rimuovere foto",
					    "Errore",
					    JOptionPane.ERROR_MESSAGE);
			}
			}
			else {
				JOptionPane.showMessageDialog(null,
					    "Album vuoto impossibile rimuovere foto",
					    "Errore",
					    JOptionPane.ERROR_MESSAGE);
			}
		}//fine rimuovi foto
		if(e.getActionCommand().equals("Avvia Slideshow")) {
			if(ab.onlyCatPwd())
			{
				JOptionPane.showMessageDialog(null,
					    "Sono Presenti solo categorie con la password",
					    "Errore",
					    JOptionPane.ERROR_MESSAGE);
			}
			else if(ab.getSizeAlbum()==0) {
				JOptionPane.showMessageDialog(null,
					    "Album vuoto impossibile iniziare slideshow",
					    "Errore",
					    JOptionPane.ERROR_MESSAGE);
			}
			else {
				Object[] option= {"OK"};
		    	JLabel lb = new JLabel("Scegli la velocit� slideshow");
		    	JPanel panel = new JPanel();
		    	panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		    	JSlider slider = new JSlider();
		    	JTextField speed = new JTextField();
		    	JButton btt = new JButton("Update");
		    	NumberFormat longformat = NumberFormat.getIntegerInstance();
		    	NumberFormatter numberFormatter = new NumberFormatter(longformat);
		    	numberFormatter.setAllowsInvalid(false);
		    	JFormattedTextField userspeed = new JFormattedTextField();
		    	userspeed.addAncestorListener(new FocusListener());
		    	try {
					MaskFormatter datemask = new MaskFormatter("#.#");
					datemask.install(userspeed);
				} catch (ParseException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
		    	userspeed.setText("5.5");
		    	slider.setAlignmentX(Component.CENTER_ALIGNMENT);
		    	speed.setAlignmentX(Component.CENTER_ALIGNMENT);
		    	btt.setAlignmentX(Component.CENTER_ALIGNMENT);
		    	userspeed.setAlignmentX(Component.CENTER_ALIGNMENT);
		    	lb.setAlignmentX(Component.CENTER_ALIGNMENT);
		    	userspeed.setPreferredSize(new Dimension(50,25));
		    	userspeed.setMaximumSize(new Dimension(50,25));
		    	speed.setAlignmentY(Component.CENTER_ALIGNMENT);
		    	speed.setEditable(false);
		    	speed.setHorizontalAlignment(SwingConstants.CENTER);
		    	speed.setBorder(BorderFactory.createEmptyBorder());
		    	slider.setMinimum(5);
		    	slider.setMaximum(100);
		    	slider.setValue(55);
		    	speed.setText("Velocit� tra una foto e l'altra: "+ (double)slider.getValue()/10 +"s");
		    	slider.setMajorTickSpacing(5);
		    	slider.setMinorTickSpacing(1);
		    	slider.setPaintTicks(true);
		    	slider.setPaintLabels(true);
		    	panel.add(lb, BorderLayout.NORTH);
		    	panel.add(slider, BorderLayout.CENTER);
		    	panel.add(speed, BorderLayout.SOUTH);
		    	panel.add(userspeed, BorderLayout.SOUTH);
		    	panel.add(btt, BorderLayout.SOUTH);
		    	panel.setPreferredSize(new Dimension(500,150));
		    	slider.addChangeListener(new ChangeListener() {

					@Override
					public void stateChanged(ChangeEvent ce) {
						DecimalFormat df = new DecimalFormat("#.#");
						String value = df.format(slider.getValue()*0.1);
						speed.setText("Velocit� tra una foto e l'altra: " + value+"s");
					}
		    		
		    	});
		    	
		    	btt.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent ae) {
						if(userspeed.getText().equals(" . "))
							userspeed.setText("5.5");
						double value = Double.parseDouble(userspeed.getText());
						slider.setValue((int) (value/0.1));
					}
		    		
		    	});
		    	
		    	
		    	int ret = JOptionPane.showOptionDialog(null, panel, "Impostazioni Slideshow", JOptionPane.NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, option,option[0]);
		    	if(ret==0) {
		    		new Slideshow(ab, slider.getValue());
		    		}
				
			}
			
			
		}//fine slideshow
	}
	
}


