package src;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;

	/**
	 * {@link JPanel} per la scelta di un album o una categoria
	 * @see JPanel
	 * @author Alessandro Palmieri
	 *
	 */
public class PanelScelta extends JPanel {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4045564366029306680L;
	/**
	 * {@link JList} dei nomi dei file relativi agli album
	 * @see JList
	 */
	private JList<Object> list;
	
	/**
	 * {@link JList} dei nomi delle categoria
	 * @see JList
	 */
	private JList<Object> liststring;
	
	/**
	 * {@link ArrayList} delle {@link Categoria} presenti nell'album
	 * @see ArrayList
	 * @see Categoria
	 * 
	 */
	private ArrayList<Categoria> listacategorie;
	
	/**
	 * inizializza il pannello per la scelta di un album
	 * @param f lista nomi file album
	 */
	public PanelScelta(String[] f) {
		JLabel label = new JLabel("Seleziona");
		this.setLayout(new BorderLayout());
		list = new JList<Object>(f);
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		list.setSelectedIndex(0);
		JScrollPane pane = new JScrollPane(list);
		add(label, BorderLayout.NORTH);
		add(pane, BorderLayout.CENTER);
	}
	
	/**
	 * inizializza il pannello per la scelta di una categoria
	 * @param listacategorie lista categoria presenti nell'album
	 */
	public PanelScelta(ArrayList<Categoria> listacategorie) {
		this.setPreferredSize(new Dimension(400,150));
		this.listacategorie=listacategorie;
		JLabel label = new JLabel("Scegli Categoria");
		setLayout(new BorderLayout());
		String[] stringhecategorie= new String[listacategorie.size()];
		for(int i=0;i<stringhecategorie.length;i++)
		{
			stringhecategorie[i]=listacategorie.get(i).getTitolo();
		}
		liststring = new JList<Object>(stringhecategorie);
		liststring.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		liststring.setSelectedIndex(0);
		JScrollPane pane = new JScrollPane(liststring);
		add(label, BorderLayout.NORTH);
		add(pane, BorderLayout.CENTER);
	}
	
	/**
	 * Restutuisce l'album selezionato dal pannello
	 * @return nome file relativo all'album
	 */
	public String getAlbumList() {
		return (String) list.getSelectedValue();
	}
	
	/**
	 * Restituisce {@link Categoria} selezionata dalla lista
	 * 
	 * @return Categoria scelta
	 * @see Categoria
	 */
	public Categoria getCategoriaList() {
		int index = liststring.getSelectedIndex();
		return listacategorie.get(index);
	}
	
	/**
	 * Restituisce la {@link CategoriaPwd} scelta dalla lista
	 * @return categoria scelta
	 * @see CategoriaPwd
	 */
	public CategoriaPwd getCategoriaPwdList() {
		int index = liststring.getSelectedIndex();
		return (CategoriaPwd) listacategorie.get(index);
	}

}