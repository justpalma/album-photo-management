package src;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.plaf.basic.BasicArrowButton;


	/**
	 * Classe per la gestione del visualizzatore foto di una categoria. Esso viene aperto ogni volta che l'utente 
	 * vuole visualizzare il contenuto di una categoria
	 * @author Palmieri Alessandro
	 */
public class InterfacciaFoto extends JFrame implements ActionListener, ComponentListener{
	
	/**
	 * {@link JButton} utilizzati per le anteprime delle foto di una categoria
	 * @see JButton
	 */
	private JButton[] foto;
	
	/**
	 * {@link JLabel} in cui viene visualizzata la foto a dimensione intera
	 * @see JLabel
	 */
	private JLabel lb;
	
	/**
	 * {@link JButton} utilizzati per le scorrere il contenuto della categoria
	 * @see JButton
	 */
	private BasicArrowButton next, prev;
	
	/**
	 * Indice della foto visualizzata a dimensione intera
	 */
	private int index;
	
	/**
	 * {@link Categoria} da cui prendere foto da visualizzare
	 * @see Categoria
	 */
	private Categoria cat;
	
	/**
	 * {@link JPanel} in cui vengono attaccati i {@link JButton delle anteprime}
	 * @see JPanel
	 * @see JButton
	 */
	private JPanel pbtt;
	
	/**
	 * Altezza della JScrollBar
	 */
	private int hscroll;
	
	/**
	 * {@link JPanel} in cui viene attaccata la {@link #lb}
	 * @see JPanel
	 * @see #lb
	 */
	private JPanel plabel;
	
	private static final long serialVersionUID = 4690054915317466556L;
	
	/**
	 * Inizializza l'Interfacciafoto
	 * @param cat {@link Categoria} da visualizzare nel visualizzatore foto
	 * @see Categoria
	 */
	public InterfacciaFoto(Categoria cat) {
		super();
		
		this.cat=cat;		
		int h = 80;  //altezza bottoni
		this.setTitle(cat.getTitolo());

		index = 0;
		foto = new JButton[cat.getSizeCategoria()];				//array di bottoni
		
		JPanel panelFoto = new JPanel();						//pannello anteprime immagini
		JPanel p = new JPanel(new BorderLayout());				//pannello totale
		pbtt = new JPanel(new GridLayout());					//pannello bottoni next prev
		setSize(new Dimension(700,500));
		
		for(int i=0;i<cat.getSizeCategoria();i++)
		{
			foto[i] = new JButton();							//creo bottone
			foto[i].setSize(new Dimension(200,h));				//setto dimensione
			foto[i].setPreferredSize(new Dimension(200,h));		//setto dimensione iniziale
			foto[i].setMaximumSize(new Dimension(200,h));		//setto dimensione massima, in modo da avere tutti i bottoni ugualo
			foto[i].setOpaque(true);
			foto[i].setBackground(Color.WHITE);
			foto[i].setBackground(new Color(57, 53, 53));
			/*
			 * controllo il tipo dell'immagine
			 * scalo le dimensioni
			 * scalo la foto
			 * setto al foto
			 */
			int type = cat.getImage(i).getType() == 0? BufferedImage.TYPE_INT_ARGB : cat.getImage(i).getType();
			BufferedImage bim = cat.getImage(i);	//leggo immagine
			Dimension dim = Gfoto.getScaledDimension(new Dimension(bim.getWidth(),bim.getHeight()), foto[i].getSize());
			bim = Gfoto.resizeImageWithHint(cat.getImage(i), type, dim.width, dim.height);
			ImageIcon i2 = new ImageIcon(bim);
			foto[i].setIcon(i2);
			foto[i].setName(cat.getNomeFoto(i));
			panelFoto.add(foto[i]);
			foto[i].addActionListener(this);	
		}
		next = new BasicArrowButton(BasicArrowButton.EAST);
		prev = new BasicArrowButton(BasicArrowButton.WEST);
		next.addActionListener(this);
		prev.addActionListener(this);
		pbtt.add(prev);
		pbtt.add(next);
		lb = new JLabel();
		plabel = new JPanel();
		plabel.setLayout(new GridBagLayout());
		plabel.add(lb);
		plabel.setBackground(Color.BLACK);
		panelFoto.setBackground(Color.black);
		JScrollPane scroll = new JScrollPane(panelFoto);
		scroll.setBorder(BorderFactory.createEmptyBorder());
		scroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_NEVER);
		p.add(pbtt,BorderLayout.NORTH);
		p.add(scroll,BorderLayout.SOUTH);
		p.add(plabel, BorderLayout.CENTER);
		
		add(p);
		scroll.setPreferredSize(new Dimension(getWidth(),h+27));
		
		setLocationRelativeTo(null);		
		setResizable(true);
		setVisible(true);	
		hscroll=scroll.getHorizontalScrollBar().getHeight();
		setFoto();
		addComponentListener(this);
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		for(int q=0;q<foto.length;q++)
		{
			if(e.getSource()==foto[q]) {
				index=q;
				if(Gfoto.getExtension(cat.getNomeFoto(q)).equals("gif"))
					setGif(q);
				else
	
					setFoto();
			}
		}
		
		if(e.getSource()==next) {
			if((index+1)==cat.getSizeCategoria())
				index=0;
			else 
				index++;
			if(Gfoto.getExtension(foto[index].getName()).equals("gif"))
				setGif(index);
			else
				setFoto();
		}
		if(e.getSource()==prev) {
			if((index-1)<0)
				index=cat.getSizeCategoria()-1;
			else
				index--;
			if(Gfoto.getExtension(foto[index].getName()).equals("gif"))
				setGif(index);
			else
				setFoto();
		}	
	}
	
	@Override
	public void componentResized(ComponentEvent e) {
		// TODO Auto-generated method stub
		if(e.getSource().getClass().getSimpleName().equals("InterfacciaFoto")) {
			if(Gfoto.getExtension(foto[index].getName()).equals("gif"))
				setGif(index);
			else
				setFoto();
		}	
	}

	@Override
	public void componentMoved(ComponentEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void componentShown(ComponentEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void componentHidden(ComponentEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	/**
	 * Imposta la foto nella {@link #lb}
	 * @see #lb
	 */
	private void setFoto() {
		int w = getWidth();	//calcolo larghezza frame
		int h = this.getHeight()-pbtt.getHeight()-hscroll;		//calcolo altezza label
		plabel.setMaximumSize(new Dimension(w, h));		//setto dimensioni label
		//contollo il tipo dell'immagine, se non esiste lo setto di default
		int type = cat.getImage(index).getType() == 0? BufferedImage.TYPE_INT_ARGB : cat.getImage(index).getType();
		BufferedImage bim = cat.getImage(index);	//leggo immagine
		//scalo le dimensioni  in base a quelle del frame
		Dimension dim = Gfoto.getScaledDimension(new Dimension(bim.getWidth(),bim.getHeight()), plabel.getSize());
		//scalo la foto 
		bim = Gfoto.resizeImageWithHint(cat.getImage(index), type, dim.width, dim.height);
		ImageIcon i2 = new ImageIcon(bim);
		lb.setIcon(i2);
		
	}
	
	/**
	 * Imposta la gif nella {@link #lb}
	 * @param q indice della gif
	 * @see #lb
	 */
	private void setGif(int q) {
		String prova = null;
		try {
			prova = new File("resources"+FileSystems.getDefault().getSeparator()+"foto").getCanonicalPath();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(new File(prova).isDirectory()==false) {
			try {
				prova=new File("src"+FileSystems.getDefault().getSeparator()+"resources"+FileSystems.getDefault().getSeparator()+"foto").getCanonicalPath();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	//necessario se eseguo da eclipse
		}
		ImageIcon ii = new ImageIcon(prova + FileSystems.getDefault().getSeparator() + cat.getNomeFoto(q));
		lb.setIcon(ii);
	}

}