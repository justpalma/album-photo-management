package src;

import java.awt.BorderLayout;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridBagLayout;
import java.awt.LayoutManager;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

	/**
	 * Interffacia in cui vengono calcolate e salvate le dimensioni utilizzate per lo slideshow
	 * @author Alessandro Palmieri
	 *
	 */
interface MyDimension {
	/**
	 * Dimension schermo 
	 */
	Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	/**
	 * Larghezza che avr� lo slideshow
	 */
	int w = (int)(screenSize.getWidth()/8)*6;
	/**
	 * Altezza che avr� lo slideshow
	 */
	int h = (int)(screenSize.getHeight()/8)*6;
}

	/**
	 * Classe specializzata di {@link TimerTask} che permette di visualizzare tutte le foto presenti nell'album e non protette da password
	 * a intervalli di tempo regolari scelti dall'utente
	 * @see TimerTask
	 * @author Alessandro Palmieri
	 */
public class Slideshow extends TimerTask implements MyDimension, ChangeListener{
	
	/**
	 * {@link JFrame} in cui viene visualizzato lo slideshow
	 * @see JFrame
	 */
	JFrame frame;
	/**
	 * {@link ArrayList} di {@link BufferedImage} di immagini da visualizzare
	 * @see ArrayList
	 * @see BufferedImage
	 */
	ArrayList<BufferedImage> arrayimm;
	/**
	 * {@link ArrayList} di nomi delle immagini da visualizzare
	 * @see ArrayList
	 */
	ArrayList<String> arrayname;
	/**
	 * indice foto visualizzata
	 */
	private int index;
	
	
	/**
	 * inizializza lo slideshow 
	 * @param ab album da cui prendere le foto
	 * @param valueslider intervallo di tempo tra una foto e l'altra, scelto dall'utente
	 */
	public Slideshow(Album ab, int valueslider) {
		long intervallo = (long) (valueslider*100);
		index=0;
		frame=new JFrame("Slideshow");
		PanelSlideshow plabel = new PanelSlideshow(new GridBagLayout());  //pannello in cui metto la label
		JPanel ptot = new JPanel(new BorderLayout());
		JTextField fieldinfo = new JTextField();
		fieldinfo.setEditable(false);
		fieldinfo.setHorizontalAlignment(JTextField.CENTER);
		fieldinfo.setBorder(BorderFactory.createEmptyBorder());
		fieldinfo.setOpaque(false);
		JLabel lb = new JLabel();
		arrayimm = new ArrayList<BufferedImage>();
		arrayname = new ArrayList<String>();
		for(int i=0;i<ab.getSizeAlbum();i++) {
			if(ab.getCategoria(i) instanceof CategoriaPwd) continue;
			for(int h=0;h<ab.getCategoria(i).getSizeCategoria();h++) {
				arrayimm.add(ab.getCategoria(i).getImage(h));
				arrayname.add(ab.getCategoria(i).getNomeFoto(h));
			}
		}
		plabel.add(lb);
		ptot.add(plabel, BorderLayout.CENTER);
		ptot.add(fieldinfo, BorderLayout.SOUTH);
		frame.add(ptot);
		
		
		frame.setSize(new Dimension(w,h));
		frame.setMaximumSize((new Dimension(w,h)));
		frame.setMaximumSize((new Dimension(w,h)));
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		Timer timer = new Timer();
		long delay=0;
		
		TimerTask task = new TimerTask() {

			@Override
			public void run() {
				checkindex();
				if(arrayimm.get(index).getWidth()>frame.getWidth() || arrayimm.get(index).getHeight()>frame.getHeight()) {
					Dimension dim = Gfoto.getScaledDimension(new Dimension(arrayimm.get(index).getWidth(),arrayimm.get(index).getHeight()), frame.getSize());
					int type = arrayimm.get(index).getType() == 0? BufferedImage.TYPE_INT_ARGB : arrayimm.get(index).getType();
					BufferedImage imm = Gfoto.resizeImageWithHint(arrayimm.get(index), type, dim.width, dim.height);
					arrayimm.set(index, imm);
				}
				fieldinfo.setText("Foto visualizzata: "+arrayname.get(index));
				lb.setIcon(new ImageIcon(arrayimm.get(index)));
				updateIndex();
			}
			
		};
		
		timer.scheduleAtFixedRate(task, delay, intervallo);
	}
	@Override
	public void run() {
		
		
	}
	 /**
	  * reimposta l'indice a 0 se siamo arrivati all'ultima foto
	  */
	private void checkindex() {
		if(index==arrayimm.size())
			index=0;
	}
	/**
	 * aumenta l'indice di uno
	 */
	private void updateIndex() {
			index++;
	}
	@Override
	public void stateChanged(ChangeEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
}


	/**
	 * {@link JPanel} in cui viene visualizzato lo slideshow
	 * @author Alessandro Palmieri
	 * @see JPanel
	 */
class PanelSlideshow extends JPanel implements MyDimension{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * {@link BufferedImage} immagine da visualizzare nel pannello
	 * @see BufferedImage
	 */
	private BufferedImage image;
	 /**
	  * inizializza il pannello
	  * @param layout {@link LayoutManager} del pannello
	  */
	public PanelSlideshow(LayoutManager layout) {
		super();
		this.setLayout(layout);
		String prova = null;
		try {
			prova = new File("resources"+FileSystems.getDefault().getSeparator()+"systemfoto").getCanonicalPath();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		if(new File(prova).isDirectory()==false) {
			try {
				prova = new File("src"+FileSystems.getDefault().getSeparator()+"resources"+FileSystems.getDefault().getSeparator()+"systemfoto").getCanonicalPath();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		try {
			image = ImageIO.read(new File(prova+FileSystems.getDefault().getSeparator()+"panelwallpaper.jpg"));
			Dimension dim2 = new Dimension(w+200,h+200);
			Dimension dim = Gfoto.getScaledDimension(new Dimension(image.getWidth(),image.getHeight()), dim2);
			int type = image.getType() == 0? BufferedImage.TYPE_INT_ARGB : image.getType();
			image = Gfoto.resizeImageWithHint(image, type, dim.width, dim.height);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(image, 0, 0, this);
	}
}
