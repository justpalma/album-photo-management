package src;

import java.awt.GridLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;

import javax.swing.JTextField;

	/**
	 * Classe che crea un {@link JPanel} personalizzato per l'aggiunta di una nuova categoria
	 * @see JPanel
	 * @author Palmieri Alessandro
	 *
	 */
public class PanelAggiuntaCategoria extends JPanel {

	
	private static final long serialVersionUID = -1106610119553186401L;
	
	/**
	 * {@linkplain JTextField} dove inserire nome categoria da creare
	 * @see JTextField
	 */
	JTextField nome;
	/**
	 * {@linkplain JPasswordField} dove inserire la password della categoria da creare
	 * @see JPasswordField
	 */
	JPasswordField pwd;
	/**
	 * {@link JCheckBox} per visualizzare la password
	 * @see JCheckBox
	 */
	JCheckBox box;
	/**
	 * {@linkplain JTextField} dove inserire descrizione della categoria da creare
	 * @see JTextField
	 */
	JTextField area;

	/**
	 * inizializza il pannello
	 */
	public PanelAggiuntaCategoria() {
		nome = new JTextField();
		pwd = new JPasswordField();
		box = new JCheckBox("Visualizza password");
		area = new JTextField();
		nome.addAncestorListener(new FocusListener());
		this.setLayout(new GridLayout(4,2));
		this.add(new JLabel("Titolo"));
		this.add(nome);
		this.add(new JLabel("Password"));
		this.add(pwd);
		this.add(new JLabel("Descrizione"));
		this.add(area);
		this.add(box);
		//box.setSelected(false);
		
		box.addItemListener(new ItemListener() {
		    public void itemStateChanged(ItemEvent e) {
		        if (e.getStateChange() == ItemEvent.SELECTED) {
		        	pwd.setEchoChar((char) 0);
		        } else {
		             
		             pwd.setEchoChar('*');
		        }
		    }
		});
	}
	
	/**
	 * Restituisce il nome della categoria appena creata
	 * @return nome categoria appena creata
	 */
	public String getTitle() {
		return nome.getText();
	}
	/**
	 * Restituisce la descrizione della categoria appena creata
	 * @return descrizone categoria appena creata
	 */
	public String getDescrizione() {
		return area.getText();
	}
	/**
	 * Restituisce la password della categoria appena creata
	 * @return password categoria appena creata
	 */
	public char[] getPassword() {
		return pwd.getPassword();
	}
	/**
	 * Restituisce la password della categoria appena creata sottoforma di stringa
	 * @return password categoria appena creata
	 */
	public String getPasswordString() {
		char[] temp = pwd.getPassword();
		String str = new String(temp);
		return str;
	}

}
