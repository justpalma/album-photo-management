package src;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.util.Random;
import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;


	/**
	 * {@link JPanel} personalizzato per la visualizzazione delle categorie con anteprima
	 * @author Palmieri Alessandro
	 *
	 * @param <T> accetta sia {@link Categoria} che {@link CategoriaPwd}
	 * @see JPanel
	 * @see Categoria
	 * @see CategoriaPwd
	 */
public class PanelCategorie<T> extends JPanel implements ActionListener{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8188743516768413463L;
	
	/**
	 * Dimensione schermo in cui viene lanciato il programma
	 */
	Dimension ss = Toolkit.getDefaultToolkit().getScreenSize();
	
	/**
	 * {@link JButton} in cui visualizzare anteprima
	 * @see JButton
	 */
	private JButton c;
	private T cat;
	
	/**
	 * {@link JFrame} in cui viene visualizzato l'album
	 */
	private JFrame gui;
	
	/**
	 * {@link JLabel} per il nome, la descrizione e la dimensione delle categorie
	 */
	private JLabel size,name,desc;
	
	/**
	 * inizializza il {@link JButton}, la categoria e il {@link GUI}
	 * @param c	Bottone per l'anteprima
	 * @param cat categoria relativa al pannello
	 * @param gui frame in cui attaccare il pannello
	 */
	public PanelCategorie(JButton c, T cat, JFrame gui) {
		this.c=c;
		this.cat=cat;
		this.gui=gui;
		Color c1 = new Color(255,255,255);
		Color back = new Color(215,189,226);
		Color buttonback = new Color(23,32,42);
		c.setBorder(BorderFactory.createLineBorder(c1, 4));		//bordo spesso
		c.setBackground(buttonback);
		this.setOpaque(false);
		this.setBackground(back);
		
		
		setComponent();
	}
	
	@Override
	public void paintComponent(Graphics g) {
        g.setColor(getBackground());
        Rectangle r = g.getClipBounds();
        g.fillRect(r.x, r.y, r.width, r.height);
        super.paintComponent(g);
    }
	
	/**
	 * inizializza tutti i componenti necessari
	 */
	public void setComponent() {
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		name = new JLabel();
		JLabel data = new JLabel();
		desc = new JLabel();
		size = new JLabel();
		
		desc.setMaximumSize(new Dimension((ss.width/3)-50,13));
		
		
		
		Font ft = new Font("Serif", Font.BOLD, 13);
		
		name.setForeground(Color.BLACK);
		name.setFont(ft);
		name.setText("Titolo: " + c.getName());
		
		data.setForeground(Color.BLACK);
		data.setFont(ft);
		data.setText("Creazione: " + ((Categoria) cat).getData().formatData());
		
		desc.setForeground(Color.BLACK);
		desc.setFont(ft);
		desc.setText("Descrizione: " + ((Categoria) cat).getDescrizione());
		
		size.setForeground(Color.BLACK);
		size.setFont(ft);
		size.setText("Dimensione: " + Integer.toString(((Categoria) cat).getSizeCategoria()));
		
	    c.setSize(new Dimension((ss.width/3)-10,ss.height/3));
		c.setMaximumSize(new Dimension((ss.width/3)-10,ss.height/3));
		c.setPreferredSize(new Dimension((ss.width/3)-10,ss.height/3));
		
		//img = Gfoto.getScaledImage(img, c.getWidth(), c.getHeight());
		
		name.setAlignmentX(Component.LEFT_ALIGNMENT);
		data.setAlignmentX(Component.LEFT_ALIGNMENT);
		size.setAlignmentX(Component.LEFT_ALIGNMENT);
		desc.setAlignmentX(Component.LEFT_ALIGNMENT);
		c.setAlignmentX(Component.LEFT_ALIGNMENT);
		c.addActionListener(this);
		add(c);
		add(name);
		add(desc);
		add(data);
		add(size);	
	}
	
	/**
	 * Aggiorna il pannello. Esso viene chiamato ogni volta che una foto viene aggiunta o rimossa da una categoria.
	 */
	public void updateCategoria()
	{
		int val = ((Categoria) cat).getSizeCategoria();
		size.setText("Dimensione: " + val);
		String prova = null;
		try {
			 prova = new File("resources"+FileSystems.getDefault().getSeparator()+"systemfoto").getCanonicalPath();
		} catch (IOException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		File f = new File(prova);
		if(f.isDirectory()==false) {
			try {
				prova = new File("src"+FileSystems.getDefault().getSeparator()+"resources"+FileSystems.getDefault().getSeparator()+"systemfoto").getCanonicalPath();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if(cat instanceof CategoriaPwd) {
			BufferedImage buff;
			try {
				buff = ImageIO.read(new File(prova+ FileSystems.getDefault().getSeparator() + "password.jpg"));
				int type = buff.getType() == 0? BufferedImage.TYPE_INT_ARGB : buff.getType();
				Dimension dim = Gfoto.getScaledDimension(new Dimension(buff.getWidth(),buff.getHeight()), c.getSize());
				buff = Gfoto.resizeImageWithHint(buff, type, dim.width, dim.height);
				ImageIcon i2 = new ImageIcon(buff);
				c.setIcon(i2);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
			else if(val==0)
			{
			BufferedImage bim;
			try {
				bim = ImageIO.read(new File(prova+ FileSystems.getDefault().getSeparator() + "empty.png"));
				int type = bim.getType() == 0? BufferedImage.TYPE_INT_ARGB : bim.getType();
				//scalo le dimensioni  in base a quelle del frame
				Dimension dim = Gfoto.getScaledDimension(new Dimension(bim.getWidth(),bim.getHeight()), c.getSize());
				//scalo la foto 
				bim = Gfoto.resizeImageWithHint(bim, type, dim.width, dim.height);
				ImageIcon i2 = new ImageIcon(bim);
				c.setIcon(i2);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else {
			Random rand = new Random();
			int n = rand.nextInt(((Categoria) cat).getSizeCategoria());
			BufferedImage buff = ((Categoria) cat).getImage(n);
			int type = buff.getType() == 0? BufferedImage.TYPE_INT_ARGB : buff.getType();
			Dimension dim = Gfoto.getScaledDimension(new Dimension(buff.getWidth(),buff.getHeight()), c.getSize());
			buff = Gfoto.resizeImageWithHint(buff, type, dim.width, dim.height);
			ImageIcon i2 = new ImageIcon(buff);
			c.setIcon(i2);		
		}
		
	}
	
	/**
	 * Aggiorna il nome e la descrizone della categoria
	 * @param title titolo categoria
	 * @param descrizione descrizione categoria
	 */
	public void updatePanel(String title, String descrizione) {
		name.setText("Titolo: "+title);
		desc.setText("Descrizione: "+descrizione);
	}
		
	@Override
	public void actionPerformed(ActionEvent e) {

		if(e.getSource()==c) {
			System.out.println(((Categoria) cat).getSizeCategoria());
			if(((Categoria) cat).getSizeCategoria()!=0){
				if(cat instanceof CategoriaPwd) {
				JPasswordField pwdfield = new JPasswordField();
				pwdfield.addAncestorListener(new FocusListener());
				int action = JOptionPane.showConfirmDialog(null, pwdfield, "Inserisci password", JOptionPane.OK_CANCEL_OPTION);
				if(action==JOptionPane.OK_OPTION) {
						String str = new String(pwdfield.getPassword());
					if(((CategoriaPwd)cat).getPwdString().equals(str)){
						//ho inserito password corretta
						gui.setVisible(false);
						InterfacciaFoto f = new InterfacciaFoto((CategoriaPwd)cat);
						f.addWindowListener(new WindowAdapter() {
							@Override
							public void windowClosing(WindowEvent e) {
								System.out.println("Chiudo Visualizzatore Foto");
								f.dispose();
								gui.setVisible(true);
							}
						});	
					}
					else {
						//password errata
						JOptionPane.showMessageDialog(this,
						    "Password Errata",
						    "Errore",
						    JOptionPane.ERROR_MESSAGE);
					}
					}
				}
				else {
						gui.setVisible(false);
						InterfacciaFoto f = new InterfacciaFoto((Categoria) cat);
						f.addWindowListener(new WindowAdapter() {
							@Override
							public void windowClosing(WindowEvent e) {
								System.out.println("Chiudo Visualizzatore Foto");
								f.dispose();
								gui.setVisible(true);
							}
						});	
					}
			}//fine cat!=0
			else {
				//categorai vuota
				
				JOptionPane.showMessageDialog(null,
					    "Categoria vuota, impossibile aprire visualizzatore foto",
					    "Errore",
					    JOptionPane.ERROR_MESSAGE);
					    
			}
			}//fine getsource
		}//fine actionperformerd


		
}
