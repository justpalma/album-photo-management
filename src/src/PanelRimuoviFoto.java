package src;


import java.awt.Dimension;
import java.util.ArrayList;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;

	/**
	 * {@link JPanel} personalizzato per la rimozione delle foto da una categoria
	 * @see JPanel
	 * @author Palmieri Alessandro
	 *
	 */
public class PanelRimuoviFoto extends JPanel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1536984645453267563L;
	/**
	 * {@link JList} in cui vengono aggiunti tutti i nomi delle foto
	 * @see JList
	 */
	private JList<Object> listafoto;
	
	/**
	 * Inizializza il pannello
	 * @param foto {@link ArrayList} contenente i nomi delle foto presenti in una categoria
	 */
	public PanelRimuoviFoto(ArrayList<String> foto) {
		this.setPreferredSize(new Dimension(400,150));
		listafoto = new JList<Object>(foto.toArray());
		listafoto.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		listafoto.setSelectedIndex(0);
		JScrollPane pane = new JScrollPane(listafoto);
		add(pane);
		
	}
	
	/**
	 * Restituisce gli indici delle foto da eliminare
	 * @return indici foto da eliminare
	 */
	public int[] getIndexSelected() {
		
		return listafoto.getSelectedIndices();
	}

}
