package src;

import javax.swing.JComponent;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;


	/**
	 * Classe che implementa un Listener per un generico {@link JComponent}.
	 * Essa si limita a richiedere il focus sul componenta che genera l' {@link AncestorListener}
	 * @author Alessandro Palmieri
	 * @see JComponent
	 * @see AncestorListener
	 *
	 */
public class FocusListener implements AncestorListener {
	
	/**
	 * Viaribile booleana che gestisce l'attivazione o rimozione del focus. Se true il componente avr� il focus
	 */
	private boolean rimuovilistener;
	
	
	/**
	 * Inizializza a true {@link #rimuovilistener} 
	 */
	public FocusListener() {
		this(true);
	}
	
	/**
	 * Inizializza {@link #rimuovilistener} secondo il parametro passato
	 * @param rimuovi se true il componente avr� il focus
	 */
	public FocusListener(boolean rimuovi) {
		rimuovilistener=rimuovi;
	}

	@Override
	public void ancestorAdded(AncestorEvent e) {
		JComponent component = e.getComponent();
		component.requestFocusInWindow();
		
		if(rimuovilistener)
			component.removeAncestorListener(this);
		
		
	}

	@Override
	public void ancestorMoved(AncestorEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void ancestorRemoved(AncestorEvent arg0) {
		// TODO Auto-generated method stub
		
	}

}
