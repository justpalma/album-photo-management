Unisci Categorie 

Permette di unire due categorie in una.
Per poter utilizzare questa funzionalità, nell'album
devono essere presenti almeno 2 categorie.
La prima categoria che verrà scelta cesserà di
esistere alla fine dell'unione, tutte le foto
presenti in essa saranno aggiunge alla categoria
scelta come destinazione.
Se provo ad unire una categoria con se stessa,
il programma eviterà di eseguire l'unione, 
essendo in questo caso un operazione inutile
E' possibile unire tutti i tipi di categoria.
Se entrambe le categorie non sono protette da 
password, l'unione sarà eseguita sempre.
Se la prima categoria è protetta da password e 
la seconda non è protetta da password, verrà
chiesto l'inserimento della password della 
prima categoria, se l'inserimento è giusto,
copierà la prima categoria nella seconda.
La seconda categoria non è protetta da password e
anche a fine unione essa non sarà protetta da 
password.
Se la prima categoria non ha la password e la
seconda categoria invece si, dopo l'inserimento 
corretto della password, verranno unite le categorie
e il risultato sarà la seconda categoria sempre 
protetta da password, con aggiunte le foto della 
prima categoria.
Se entrambe le categorie sono protette da password,
dopo il corretto inserimento di entrambe le password
verranno unite le categorie e il risultato sarà 
l'avere nella seconda categoria tutte le foto di
essa più quelle della prima. 
Naturalmente la categoria risultante avrà come
password quella della seconda.
