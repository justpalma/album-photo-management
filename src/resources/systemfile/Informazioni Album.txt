Informazioni Album

Visualizza tutte le caratteristiche principali
dell'album corrente:
1)Nome dell'album
2)La data e l'orario di creazione dell'album
3)Il numero di categorie totali
4)Il numero di categorie protette da una password
5)Il numero di foto presenti nell'album
