Crea Categoria

Permette di creare una nuova categoria, scegliendo se
essa deve essere protetta da password.
Se la categoria DEVE essere protetta da password,
inserire la password nella sua area di inserimento,
altrimenti lasciando questa area di testo vuota,
la categoria che verrà creata non sarà protetta da 
password.
